﻿using iCare.Commons.Entities;
using iCare.CoreAccess.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace iCare.Operator.Wpf
{
    /// <summary>
    /// Interaction logic for PatientPage.xaml
    /// </summary>
    public partial class PatientPage : Page
    {
        public PatientPage()
        {
            InitializeComponent();
            try
            {
                List<Patient> nurses = PatienRequestController.GetAllPatients();
                dataGrid_Patietns.ItemsSource = nurses;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void RefreshGrid()
        {
            List<Patient> nurses = PatienRequestController.GetAllPatients();
            dataGrid_Patietns.ItemsSource = nurses;
        }
        private void Bttn_CreateNurse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txt_Firstname.Text == "" || txt_LastName.Text == "" || txt_Password.Text == "" || txt_UserName.Text == "" || txt_Nr.Text == "")
                {
                    throw new ArgumentException("You must fill all fields");
                }
                Patient patientToCreate = new Patient();
                patientToCreate.firstname = txt_Firstname.Text;
                patientToCreate.lastname = txt_LastName.Text;
                patientToCreate.nr = txt_Nr.Text;
               // patientToCreate.user = new User() { password = txt_Password.Text, username = txt_UserName.Text };
                patientToCreate.state = true;
                //NurseRequestController.CreateNurse(patientToCreate);
                RefreshGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
