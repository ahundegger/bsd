﻿using iCare.Commons.Entities;
using iCare.CoreAccess.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace iCare.Operator.Wpf
{
    /// <summary>
    /// Interaktionslogik für CareListPage.xaml
    /// </summary>
    public partial class CareListPage : Page
    {
        public CareListPage()
        {
            InitializeComponent();
            loadCareList();
        }

        public void loadCareList()
        {
            try
            {
                List<CareList> careLists = (List<CareList>)CareListRequestController.getAllCareList();
                dataGrid_CareList.ItemsSource = careLists;
            } catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }
    }
}
