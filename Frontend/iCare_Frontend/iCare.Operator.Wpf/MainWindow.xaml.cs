﻿using iCare.CoreAccess.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace iCare.Operator.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string username = "";
        private string password = "";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Login_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.username = input_Username.Text;
                this.password = input_Password.Password;
                UsageProvider.IpAddress = txt_ipAddress.Text;
                int logInId = AdminRequestController.RequestLogin(username, password);
                if (logInId != -1)
                {
                    //login successful
                    MoveImageToRight(img_logo, 100, 0);
                    Configuration.CredentialManager.SetCredentials(username, password);
                    UsageProvider.SetCredentials(username, password);
                   
                }
                else
                {
                    MessageBox.Show("Error logging in! Maybe the username or the password is incorrect!");
                    input_Password.Password = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void MoveImageToRight(Image target, double newX, double newY)
        {
            var left = this.Width;
            TranslateTransform trans = new TranslateTransform();
            target.RenderTransform = trans;
            DoubleAnimation animation = new DoubleAnimation(0, (newY + left) - (target.Width + 10), TimeSpan.FromSeconds(1.5));
            animation.Completed += (sender, eArgs) =>
            {
                HideLoginUI();
            };
            trans.BeginAnimation(TranslateTransform.XProperty, animation);
        }

        private void ShowNavUI()
        {
            button_nav_nurses.Visibility = Visibility.Visible;
            button_nav_patients.Visibility = Visibility.Visible;
            button_nav_routes.Visibility = Visibility.Visible;
        }

        private void HideLoginUI()
        {
            button_Login.Visibility = Visibility.Hidden;
            input_Password.Visibility = Visibility.Hidden;
            input_Username.Visibility = Visibility.Hidden;
        }

        private void Button_nav_nurses_Click(object sender, RoutedEventArgs e)
        {
            this.frame_Entities.Navigate(new NursePage());
        }

        private void Bttn_BackHome_Click(object sender, RoutedEventArgs e)
        {
            this.frame_Entities.Content = null;
        }
       

        private void Button_nav_patients_Click(object sender, RoutedEventArgs e)
        {
            this.frame_Entities.Navigate(new PatientPage());
        }
    }
}
