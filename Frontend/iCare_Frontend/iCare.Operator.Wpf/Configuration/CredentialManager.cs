﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iCare.Operator.Wpf.Configuration
{
    public static class CredentialManager
    {
        private static string Username { get; set; }
        private static string Password { get; set; }

        public static void SetCredentials(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public static string GetUsername()
        {
            return Username;
        }

        public static string GetPassword()
        {
            return Password;
        }
    }
}