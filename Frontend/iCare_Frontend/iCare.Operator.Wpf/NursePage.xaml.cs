﻿using iCare.Commons.Entities;
using iCare.CoreAccess.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace iCare.Operator.Wpf
{
    /// <summary>
    /// Interaktionslogik für NursePage.xaml
    /// </summary>
    public partial class NursePage : Page
    {
        private bool isManualEditCommit;
        public NursePage()
        {
            InitializeComponent();
            try
            {
                List<Nurse> nurses = NurseRequestController.GetAllNurses();
                dataGrid_Nurses.ItemsSource = nurses;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RefreshGrid()
        {
            List<Nurse> nurses = NurseRequestController.GetAllNurses();
            dataGrid_Nurses.ItemsSource = nurses;
        }

        private void Bttn_CreateNurse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txt_Firstname.Text == "" || txt_LastName.Text == "" || txt_Password.Text == "" || txt_UserName.Text == "" || txt_SSNR.Text == "")
                {
                    throw new ArgumentException("You must fill all fields");
                }
                Nurse nurseToCreate = new Nurse();
                nurseToCreate.firstname = txt_Firstname.Text;
                nurseToCreate.lastname = txt_LastName.Text;
                nurseToCreate.ssnr = txt_SSNR.Text;
                nurseToCreate.user = new User() { password = txt_Password.Text, username = txt_UserName.Text };
                nurseToCreate.state = true;
                NurseRequestController.CreateNurse(nurseToCreate);
                RefreshGrid();
            }catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DataGrid_Nurses_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
             
            if (!isManualEditCommit)
            {
                isManualEditCommit = true;
                DataGrid grid = (DataGrid)sender;
                grid.CommitEdit(DataGridEditingUnit.Row, true);
                isManualEditCommit = false;
            }
            else {
                Nurse selectedNurse = (Nurse)dataGrid_Nurses.SelectedItem;
                MessageBox.Show("Editing Nurse and changed" + selectedNurse.firstname + " " + selectedNurse.lastname + " " + selectedNurse.ssnr + " " + selectedNurse.state);
            }
            
        }
      
    }
}
