﻿using System;

namespace iCare.CoreAccess.Authentification
{
    public static class LoginController
    {
        public static int LoginAsNurse(string username, string password)
        {
#if DEBUG
            if (username == "debug")
            {
                return 1;
            }
#endif
            return Requests.NurseRequestController.RequestLogin(username, password);
        }

        public static int LoginAsAdmin(string username, string password)
        {
#if DEBUG
            if (username == "debug")
            {
                return 1;
            }
#endif
            return Requests.AdminRequestController.RequestLogin(username, password);
        }
    }
}
