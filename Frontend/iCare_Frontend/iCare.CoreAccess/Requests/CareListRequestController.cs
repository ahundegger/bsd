﻿using iCare.Commons.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace iCare.CoreAccess.Requests
{
    public static class CareListRequestController
    {
        public static CareList GetCareListByNurseId(int nurseId)
        {
            CareList retVal = new CareList();
            string result = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + UsageProvider.IpAddress + ":8080/RestService/api/carelist/getByNurseId/" + nurseId);
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + EncoderHelper.Base64Encode(UsageProvider.GetUsername() + ":" + UsageProvider.GetPassword()));
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                Console.WriteLine(result);
                
            }catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            CareList carelist = JsonConvert.DeserializeObject<CareList>(result);
            foreach (CareListEntry ce in carelist.careListEntries)
            {
                if (ce.patient.allFindings == null)
                {
                    ce.patient.allFindings = new List<PatientFinding>();
                }
            }
            Console.WriteLine(carelist);
            return carelist;

        }


        public static IEnumerable<CareList> getAllCareList()
        {
            List<CareList> retCareList = null;
            try
            {         
                string result = "";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + UsageProvider.IpAddress + ":8080/RestService/api/carelist/");
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + EncoderHelper.Base64Encode(UsageProvider.GetUsername() + ":" + UsageProvider.GetPassword()));
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                retCareList = JsonConvert.DeserializeObject<List<CareList>>(result);
            } catch (Exception ex)
            {
                throw ex;
            }
            return retCareList;
        }
    }
}
