﻿using iCare.Commons.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace iCare.CoreAccess.Requests
{
    public static class NurseRequestController
    {
        
        public static int RequestLogin(string username, string password)
        {
            string result = "";
            int retVal = -1;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + UsageProvider.IpAddress + ":8080/RestService/api/user/login");
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + EncoderHelper.Base64Encode(username + ":" + password));
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();                    
                }
                retVal = Int32.Parse(result);
                if(Int32.Parse(result) != -1)
                {
                    UsageProvider.SetCredentials(username, password);
                }
                
            }catch(Exception ex)
            {
                retVal = -1;
                Console.WriteLine("ERROR: " + ex.Message);
            }

            return retVal;
        }

        public static List<Patient> GetDummyPatients()
        {
            List<Patient> retVal = new List<Patient>();

            for (int i = 0; i< 25; i++)
            {
                Patient p = new Patient("Klaus " + i,"Huber "+i, "P0" + i, "Das ist ein sehr netter Patient\n - MS\n - Tromb\n - Demenz\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nTrapuzzano ist der Couseng von dem DUde! Er hat die Zeitung immer in der Hand. Erstatzschlüssel ist in der Schüssel.");
                p.place = new Place("Huberterweg", 12.334, 46.435,i + "a",9500,"Villach","Austria");
                retVal.Add(p);
            }
            return retVal;
        }

        public static List<Nurse> GetAllNurses()
        {
            List<Nurse> retVal = new List<Nurse>();
            string result = "";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + UsageProvider.IpAddress + ":8080/RestService/api/nurse/");
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + EncoderHelper.Base64Encode(UsageProvider.GetUsername() + ":" + UsageProvider.GetPassword()));
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
            List<Nurse> allNurses = JsonConvert.DeserializeObject < List<Nurse>>(result);
                Console.WriteLine(allNurses);
           return allNurses;           
        
        }

        public static Nurse CreateNurse(Nurse nurseToCreate)
        {
            string result = "";
            Nurse retVal = null;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + UsageProvider.IpAddress + ":8080/RestService/api/nurse/create");
                request.Method = "POST";                
                request.ContentType = "application/json";
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(nurseToCreate);                  
                    Console.WriteLine(json);
                    streamWriter.Write(json);
                }
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + EncoderHelper.Base64Encode(UsageProvider.GetUsername() + ":" + UsageProvider.GetPassword()));
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                Console.WriteLine(result);

            }
            catch (Exception ex)
            {                
                Console.WriteLine("ERROR: " + ex.Message);
                throw ex;
            }
            retVal = JsonConvert.DeserializeObject<Nurse>(result);
            return retVal;
        }

        public static bool SubmitTimeTracking(int nurseId, int patientId, DateTime startStamp, DateTime endStamp)
        {
            return false;
        }
    }
}
