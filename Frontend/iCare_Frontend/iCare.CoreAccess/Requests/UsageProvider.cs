﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iCare.CoreAccess.Requests
{
   public static class UsageProvider
    {
        public  static string IpAddress { get; set; }

        private static string Username { get; set; }
        private static string Password { get; set; }

        public static void SetCredentials(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public static string GetUsername()
        {
            return Username;
        }

        public static string GetPassword()
        {
            return Password;
        }

    }
}
