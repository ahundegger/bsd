﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace iCare.CoreAccess.WebService
{
    public static class RequestController
    {
        /// <summary>
        /// For test purposes only
        /// </summary>
        /// <returns></returns>
        public static string RequestHelloWorld()
        {
            string result = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://192.168.192.143:8080/08_Madritsch_Servlet_HelloWorld/ServHello");
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }
            Console.WriteLine(result);
            return result;
        }
    }
}
