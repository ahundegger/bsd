﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;


namespace iCare.CoreAccess.Requests
{
    public static class AdminRequestController
    {
        public static int RequestLogin(string username, string password)
        {
             
            string result = "";
            int retVal = -1;
            try
            {                
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://"+UsageProvider.IpAddress+":8080/RestService/api/user/login");
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + EncoderHelper.Base64Encode(username + ":" + password));
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                retVal = Int32.Parse(result);
            }catch(Exception)
            {
                retVal = -1;
            }

            return retVal;
        }
    }
}
