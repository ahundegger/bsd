﻿using iCare.Commons.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace iCare.CoreAccess.Requests
{
    public static class PatienRequestController
    {
        public static List<Patient> GetAllPatients()
        {
            List<Patient> retVal = new List<Patient>();
            string result = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + UsageProvider.IpAddress + ":8080/RestService/api/patient/");
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + EncoderHelper.Base64Encode(UsageProvider.GetUsername() + ":" + UsageProvider.GetPassword()));
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }
            List<Patient> allPatients = JsonConvert.DeserializeObject<List<Patient>>(result);
            Console.WriteLine(allPatients);
            return allPatients;
        }

        public static Patient UpdatePatient(Patient patientToUpdate)
        {
            Patient retVal = new Patient();
            string result = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + UsageProvider.IpAddress + ":8080/RestService/api/patient/update/");
                request.Method = "PUT";
                request.ContentType = "application/json";
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(patientToUpdate);
                    json = json.Replace("false", "0");
                    Console.WriteLine(json);
                    streamWriter.Write(json);
                }
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + EncoderHelper.Base64Encode(UsageProvider.GetUsername() + ":" + UsageProvider.GetPassword()));
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                Console.WriteLine(result);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            Patient patient = JsonConvert.DeserializeObject<Patient>(result);
            Console.WriteLine(patient);
            return patient;

        }
    }
}
