﻿using Android.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace iCare.Commons.Entities
{
    public class PatientFinding
    {
        public int Id { get; private set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public byte[] Image { get; set; }

        public PatientFinding(string name, DateTime date)
        {
            this.Name = name;
            this.Date = date;
        }

        public PatientFinding(string name, DateTime date, byte[] image)
        {
            this.Name = name;
            this.Date = date;
            this.Image = image;
        }

        public Bitmap GetImageAsAndroidBitmap()
        {
            return BitmapFactory.DecodeByteArray(this.Image, 0, this.Image.Length);
        }
    }
}
