﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iCare.Commons.Entities
{
    public class Place
    {
        public int id { get;  set; }
        public string streetName { get; set; }
        public string houseNr { get; set; }
        public int postalCode { get; set; }
        public string countryName { get; set; }
        public string cityName { get; set; }
        public double coordX { get; set; }
        public double coordY { get; set; }   
        public string description { get; set; }

        public Place(string street, double X, double Y, string houseNr, int postalCode, string city, string country)
        {
            this.houseNr = houseNr;
            this.cityName = city;
            this.countryName = country;
            this.postalCode = postalCode;
            this.streetName = street;
            this.coordX = X;
            this.coordY = Y;
        }

        public Place()
        {

        }

        public string ToDisplayString()
        {
            return $"{this.streetName} {this.houseNr}\n{this.cityName}, {this.postalCode}\n{this.countryName}";
        }
    }
}
