﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iCare.Commons.Entities
{
    public class Nurse
    {

        public string firstname { get; set; }

        public string lastname { get; set; }

        public string ssnr { get; set; }

        public int id { get; set; }

        public User user { get; set; }

        public bool state { get; set; }
    }
}
