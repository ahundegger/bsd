﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iCare.Commons.Entities
{
   public class CareList
    {       

        public int id { get; set; }
        public string name { get; set; }
        public bool state { get; set; }
        public Nurse assigned { get; set; }
        public List<CareListEntry> careListEntries { get; set; }


    }
}
