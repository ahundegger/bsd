﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iCare.Commons.Entities
{
    public class CareListEntry
    {       

        public int id { get; set; }

        public bool state { get; set; }

        public Patient patient { get; set; }

    }
}
