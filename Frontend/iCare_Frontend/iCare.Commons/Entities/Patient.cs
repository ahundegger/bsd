﻿using Android.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace iCare.Commons.Entities
{
    public class Patient
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string nr { get; set; }
        public string notes { get; set; }
        public int id { get; set; }
        public List<PatientFinding> allFindings { get; set; }
        public Place place { get; set; }
        public bool state { get; set; }
        public byte[] profileImage { get; set; }

        public Patient(string firstname,string lastname, string nr, string notes)
        {
            this.nr = nr;
            this.notes = notes;
            this.firstname = firstname;
            this.lastname = lastname;
            this.allFindings = new List<PatientFinding>();
        }

        public Patient(string name, string nr, string notes, Place location)
        {
            this.nr = nr;
            this.notes = notes;
            this.firstname = firstname;
            this.lastname = lastname;
            this.place = location;
            this.allFindings = new List<PatientFinding>();
        }

        public string FullName()
        {
            return firstname + " " + lastname;
        }
        public Bitmap GetImageAsAndroidBitmap()
        {
            return BitmapFactory.DecodeByteArray(this.profileImage, 0, this.profileImage.Length);
        }

        public Patient()
        {
            this.allFindings = new List<PatientFinding>();
        }
    }
}
