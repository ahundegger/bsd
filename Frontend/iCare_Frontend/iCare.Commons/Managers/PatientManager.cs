﻿using iCare.Commons.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace iCare.Commons.Managers
{
    public class PatientManager
    {
        public static List<Patient> _patients = new List<Patient>();

        public static void Test_GeneratePatients(int patientCount)
        {
            Patient p;
            for (int i=1;i<=patientCount;i++)
            {
                p = new Patient("Patient " + i,"Patient Lastname"+i, "P0"+i,"Testing notes for patient P0" + i + "\n medication: Praxiten 1|0|0");
                _patients.Add(p);
            }
        }

        public static IEnumerable<Patient> GetPatients()
        {
            List<Patient> retVal = new List<Patient>();
            foreach (Patient p in _patients)
            {
                retVal.Add(p);
            }
            return retVal;
        }
    }
}
