﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace iCare.Client.Droid.Logger
{
    public static class ConsLogger
    {
        public static void LogToConsole(object sender, string message)
        {
            Console.WriteLine("ConsLogger -> " + sender.GetType().Name + " : " + message);
        }

        public static void LogExceptionToConsole(object sender, Exception ex)
        {
            Console.WriteLine("ConsLogger -> " + sender.GetType().Name + " [ " + ex.GetType().Name + "] : " + ex.Message);
        }
    }
}