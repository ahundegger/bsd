﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using iCare.Client.Droid.Exceptions;
using Java.IO;

namespace iCare.Client.Droid.Services
{
    public static class TimeTrackingService
    {
        public static string GetStartTime(int patientId)
        {
            string retVal = "";
            Java.IO.File sdCard = Android.OS.Environment.ExternalStorageDirectory;
            Java.IO.File dir = new Java.IO.File(sdCard.AbsolutePath + "/nurse");
            dir.Mkdirs();
            Java.IO.File file = new Java.IO.File(dir, "timestamp.txt");
            if (!file.Exists())
            {
                return "";
            }
            else
            {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                string currLine;
                while ((currLine = reader.ReadLine()) != null)
                {
                    string[] pair = currLine.Split(';');
                    int patient = Convert.ToInt32(pair[0]);
                    if (patient==patientId)
                    {
                        retVal = pair[1];
                    }
                    else
                    {
                        throw new TimeTrackingException("Time is already being tracked at another patient!");
                    }
                }
                return retVal;
            }
        }

        public static void DeleteFile()
        {
            Java.IO.File sdCard = Android.OS.Environment.ExternalStorageDirectory;
            Java.IO.File dir = new Java.IO.File(sdCard.AbsolutePath + "/nurse");
            dir.Mkdirs();
            Java.IO.File file = new Java.IO.File(dir, "timestamp.txt");
            if (file.Exists())
            {
                file.Delete();
            } else
            {
                throw new Exception("Could not delete timestamp-file, file does not exist!");
            }
        }

        public static void SetStartTime(int patientId, string timeStamp)
        {
            try
            {
                Java.IO.File sdCard = Android.OS.Environment.ExternalStorageDirectory;
                Java.IO.File dir = new Java.IO.File(sdCard.AbsolutePath + "/nurse");
                dir.Mkdirs();
                Java.IO.File file = new Java.IO.File(dir, "timestamp.txt");
                if (!file.Exists())
                {
                    file.CreateNewFile();
                    file.Mkdir();
                    FileWriter writer = new FileWriter(file);

                    writer.Write(patientId + ";" + timeStamp);

                    writer.Flush();
                    writer.Close();
                }
                else
                {
                    throw new TimeTrackingException("Time is already being tracked at another patient!");
                }
            }
            catch (TimeTrackingException te)
            {
                throw te;
            }
            catch (Exception ex)
            {
                throw new Exception("Error writing timestamp to storage. Please contact support!", ex);
            }
        }
    }
}