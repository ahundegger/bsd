﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;

namespace iCare.Client.Droid.Cache
{
    public static class SettingsCache
    {
        public static bool isEstimatedTimeOn { get; set; }

        public static bool ReadSettings()
        {
            Java.IO.File sdCard = Android.OS.Environment.ExternalStorageDirectory;
            Java.IO.File dir = new Java.IO.File(sdCard.AbsolutePath + "/nurse");
            dir.Mkdirs();
            Java.IO.File file = new Java.IO.File(dir, "settings.txt");                        
            if (!file.Exists())
            {
                file.CreateNewFile();
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                string contentToWrite = "estimatedTime=TRUE";
                writer.Write(contentToWrite);                
                writer.Close();
                ReadSettings();
                //throw new Exception("Unable to read settings file, because settings.txt does not exist yet.");
            }
            else
            {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                string currLine;
                while ((currLine = reader.ReadLine()) != null)
                {
                    string[] pair = currLine.Split('=');
                    string key = pair[0];
                    string value = pair[1];
                    switch (key)
                    {
                        case "estimatedTime":
                            if (value.ToUpper() == "TRUE")
                            {
                                isEstimatedTimeOn = true;
                                break;
                            }
                            else
                            {
                                isEstimatedTimeOn = false;
                                break;
                            }
                    }
                }
            }
            return true;
        }
    }
}