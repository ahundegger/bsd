﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using iCare.Client.Droid.Cache;
using iCare.Client.Droid.Views.ViewHolders;
using iCare.Commons.Entities;

namespace iCare.Client.Droid.Adapter
{
    public class NurseryListAdapter : RecyclerView.Adapter
    {
        //public List<Patient> items;
        public CareList items;

        public event EventHandler<int> ItemClick;

        //public NurseryListAdapter(List<Patient> patients)
        //{
        //    this.items = patients;
        //}
        public NurseryListAdapter(CareList patients)
        {
            this.items = patients;
        }

        public Patient GetPatient(int position)
        {
            return this.items.careListEntries[position].patient;
        }

        void OnClick(int position)
        {
            if (this.ItemClick != null)
                this.ItemClick(this, position);
        }

        public override int ItemCount => this.items.careListEntries?.Count ?? 0;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            try
            {            
            NurseryListViewHolder vh = holder as NurseryListViewHolder;
            Patient currPatient = this.items.careListEntries[position].patient;
            vh.Name.Text = currPatient.firstname+ " "+ currPatient.lastname;
            vh.Address.Text = currPatient.place.description;
            if (!SettingsCache.isEstimatedTimeOn)
            {
                vh.EstimatedTime.Visibility = ViewStates.Gone;
            } else
            {
                vh.EstimatedTime.Visibility = ViewStates.Visible;
            }
            vh.EstimatedTime.Text = "20 min"; //DEBUG
#if DEBUG 
            if (currPatient.profileImage == null)
            {
                vh.Image.SetImageResource(Resource.Drawable.laugoldman);
            }
            else
            {
                vh.Image.SetImageBitmap(currPatient.GetImageAsAndroidBitmap());
            }
#endif
            } catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                   Inflate(Resource.Layout.PatientCardView, parent, false);
            NurseryListViewHolder vh = new NurseryListViewHolder(itemView, this.OnClick);
            return vh;
        }
    }
}