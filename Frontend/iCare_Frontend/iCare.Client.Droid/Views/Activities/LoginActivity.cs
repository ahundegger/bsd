﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using System;
using Android.Content.PM;
using Android.Content.Res;
using Android;
using Android.Util;
using Android.Support.Design.Widget;
using iCare.Client.Droid.Cache;

namespace iCare.Client.Droid
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class LoginActivity : AppCompatActivity
    {
        private EditText edit_Username;
        private EditText edit_Password;
        private EditText edit_IPAddress;

        private LinearLayout root;

        public string TAG { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            this.TAG = this.GetType().ToString();
            // Set our view from the "login" layout resource
            SetContentView(Resource.Layout.activity_login);

            root = this.FindViewById<LinearLayout>(Resource.Id.root_login);

            var Button_Login = this.FindViewById<Button>(Resource.Id.btn_login);
            //add the login event to the click event of the login button
            Button_Login.Click += TryLogin;

            edit_Username = this.FindViewById<EditText>(Resource.Id.input_txt_username);
            edit_Password = this.FindViewById<EditText>(Resource.Id.input_txt_password);
            edit_IPAddress = this.FindViewById<EditText>(Resource.Id.input_txt_IPAddress);
#if DEBUG
            edit_Username.Text = "madritsa";
            edit_Password.Text = "1234";
            edit_IPAddress.Text = "192.168.1.17";
#endif
        }

        private void TryLogin(object sender, EventArgs e)
        {
            string username = edit_Username.Text;
            string password = edit_Password.Text;
            CoreAccess.Requests.UsageProvider.IpAddress = edit_IPAddress.Text;
            
            int idNurse = CoreAccess.Authentification.LoginController.LoginAsNurse(username, password);         
            if (idNurse != -1)
            {
                //Save credentials for example in setup-class, ...
                Configuration.CredentialManager.SetCredentials(username, password, idNurse);
                SettingsCache.ReadSettings();
                Android.Support.V7.App.AlertDialog.Builder alert = new Android.Support.V7.App.AlertDialog.Builder(this);
                this.StartActivity(typeof(MainActivity));
                Dialog dialog = alert.Create();
                dialog.Show();
            }
            else
            {
                //Display an alert that the username or password could be false
                Android.Support.V7.App.AlertDialog.Builder alert = new Android.Support.V7.App.AlertDialog.Builder(this);
                alert.SetTitle("Could not login!");
                alert.SetIcon(Resource.Drawable.icare_logo192);
                alert.SetMessage("Maybe the username or the password is incorrect!");
                alert.SetPositiveButton("OK", (senderAlert, args) =>
                {
                });
                Dialog dialog = alert.Create();
                dialog.Show();
                //set content of password-input to empty string
                edit_Password.Text = "";
            }
        }
    }
}