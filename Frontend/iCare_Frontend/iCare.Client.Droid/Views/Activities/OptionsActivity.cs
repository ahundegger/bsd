﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using iCare.Client.Droid.Cache;
using Java.IO;

namespace iCare.Client.Droid.Views.Activities
{
    [Activity(Label = "OptionsActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class OptionsActivity : Activity
    {
        private bool settingsChanged = false;
        private Switch switch_EstimatedTime;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.activity_options);

            this.switch_EstimatedTime = FindViewById<Switch>(Resource.Id.switch_estimedTimeDisplay);
            this.switch_EstimatedTime.CheckedChange += delegate (object sender, CompoundButton.CheckedChangeEventArgs e) {
                settingsChanged = true;
            };
            this.switch_EstimatedTime.Checked = SettingsCache.isEstimatedTimeOn;

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar_options);
            SetActionBar(toolbar);
            ActionBar.Title = "Settings";
            ActionBar.SetHomeButtonEnabled(true);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menuitems_options, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    return true;
                default:
                    SaveSettings();
                    return base.OnOptionsItemSelected(item);
            }
        }

        public void SaveSettings()
        {
            //Runtime Permission checks
            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == (int)Permission.Granted && ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) == (int)Permission.Granted)
            {
                Snackbar snackbar = Snackbar.Make(this.FindViewById<LinearLayout>(Resource.Id.options_root), "Saving changes...", Snackbar.LengthLong);
                snackbar.Show();
                //saving settings and the values to a file on the local_storage of the device
                try
                {
                    Java.IO.File sdCard = Android.OS.Environment.ExternalStorageDirectory;
                    Java.IO.File dir = new Java.IO.File(sdCard.AbsolutePath + "/nurse");
                    dir.Mkdirs();
                    Java.IO.File file = new Java.IO.File(dir, "settings.txt");
                    if (!file.Exists())
                    {
                        file.CreateNewFile();
                        file.Mkdir();
                        FileWriter writer = new FileWriter(file);

                        writer.Write(GetSettingsKeyValuePairAsString());

                        writer.Flush();
                        writer.Close();
                    }
                    else
                    {
                        file.Delete();

                        file.CreateNewFile();
                        file.Mkdir();
                        FileWriter writer = new FileWriter(file);

                        writer.Write(GetSettingsKeyValuePairAsString());

                        writer.Flush();
                        writer.Close();
                    }

                    Snackbar snackbar_finished = Snackbar.Make(this.FindViewById<LinearLayout>(Resource.Id.options_root), "Changes saved successfuly...", Snackbar.LengthLong);
                    snackbar_finished.Show();
                    this.settingsChanged = false;
                    SettingsCache.ReadSettings();
                }
                catch (Exception ex)
                {
                    Android.Support.V7.App.AlertDialog.Builder alert = new Android.Support.V7.App.AlertDialog.Builder(this);
                    alert.SetTitle("Error saving settings:");
                    alert.SetIcon(Resource.Drawable.icare_logo192);
                    alert.SetMessage(ex.Message);
                    alert.SetPositiveButton("OK", (senderAlert, args) =>
                    {
                    });
                    Dialog dialog = alert.Create();
                    dialog.Show();
                }
            }
            else
            {
                Snackbar snackbar_finished = Snackbar.Make(this.FindViewById<LinearLayout>(Resource.Id.options_root), "No permission to read or write external storage :( ", Snackbar.LengthLong);
                snackbar_finished.Show();
            }
        }

        private string GetSettingsKeyValuePairAsString()
        {
            string retVal = "";
            retVal += "estimatedTime";
            retVal += "=";
            //Estimated Time Switcher Evaluation
            if (switch_EstimatedTime.Checked)
            {
                retVal += "true";
            } else
            {
                retVal += "false";
            }
            retVal += "\n";
            return retVal;
        }

        public override void OnBackPressed()
        {
            
            if (settingsChanged)
            {
              
                Android.Support.V7.App.AlertDialog.Builder alert = new Android.Support.V7.App.AlertDialog.Builder(this);
                alert.SetTitle("Leave Settings?");
                alert.SetIcon(Resource.Drawable.icare_logo192);
                alert.SetMessage("Do you really want to leave the Settings? Maybe there are unsaved changes to the settings you may want to save!");
                alert.SetPositiveButton("LEAVE", (senderAlert, args) =>
                {
                    base.OnBackPressed();
                });
                alert.SetNegativeButton("STAY", (senderAlert, args) =>
                {
                });
                Dialog dialog = alert.Create();
                dialog.Show();
            }
            else
            {
                base.OnBackPressed();
            }
        }
    }
}