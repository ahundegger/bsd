﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace iCare.Client.Droid.Views.Activities
{
    [Activity(Label = "FindingActivity")]
    public class FindingActivity : Activity
    {
        private ImageView image;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_finding);

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar_finding);
            SetActionBar(toolbar);
            ActionBar.Title = "Finding";
            ActionBar.SetHomeButtonEnabled(true);
            ActionBar.SetDisplayHomeAsUpEnabled(true);

            this.image = FindViewById<ImageView>(Resource.Id.img_Finding);
            Bitmap bitmap = (Bitmap) Intent.GetParcelableExtra("image");
            this.image.SetImageBitmap(bitmap);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}