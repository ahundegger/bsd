﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using iCare.Client.Droid.Exceptions;
using iCare.Client.Droid.Services;
using iCare.Commons.Entities;
using iCare.CoreAccess.Requests;
using Newtonsoft.Json;

namespace iCare.Client.Droid.Views.Activities
{
    [Activity(Label = "PatientDashboardActivity")]
    public class PatientDashboardActivity : Activity
    {
        private Patient patient;
        private TextView txt_patientName;
        private TextView txt_patientAddress;
        private ImageView img_patient;
        private EditText patientDesc;
        private Button btn_TimeTracking;
        private bool isCurrTrackingTime = false;
        private ImageView btn_Camera;
        private LinearLayout findingGallery;
        private TextView textViewFindingsCount;
        private string startedTimestamp = "";
        private TextView timeDisplay;
        private bool isTrackingOtherPatient;

        public class AddFindingDialog : AlertDialog
        {
            public Button btn_Add;
            public Button btn_Cancel;
            public ImageView img_Finding;
            public Bitmap currFindingImage;

            public AddFindingDialog(Activity activity) : base(activity)
            {
            }

            public AddFindingDialog(Activity activity, Bitmap image) : base(activity)
            {
                this.currFindingImage = image;
            }

            protected override void OnCreate(Bundle savedInstanceState)
            {
                base.OnCreate(savedInstanceState);
                this.SetContentView(Resource.Layout.AddFindingDialog);

                btn_Cancel = FindViewById<Button>(Resource.Id.btn_CancelFinding);
                btn_Add = FindViewById<Button>(Resource.Id.btn_AddFinding);
                img_Finding = FindViewById<ImageView>(Resource.Id.image_NewFinding);
                img_Finding.SetImageBitmap(this.currFindingImage);

            }
        }

        public PatientDashboardActivity()
        {

        }

        public void GetTimeTrackingInfo()
        {
            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == (int)Permission.Granted && ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) == (int)Permission.Granted)
            {
                try
                {
                    this.startedTimestamp = TimeTrackingService.GetStartTime(this.patient.id);
                    this.isTrackingOtherPatient = false;
                }
                catch (TimeTrackingException)
                {
                    this.isTrackingOtherPatient = true;
                }
            }
            else
            {
                Snackbar snackbar_finished = Snackbar.Make(this.FindViewById<LinearLayout>(Resource.Id.dash_root), "Permission to read/write local storage need to save timestamps. :(", Snackbar.LengthLong);
                snackbar_finished.Show();
            }
        }

        public void SetStartTimeTracking()
        {
            try
            {
                if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == (int)Permission.Granted && ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) == (int)Permission.Granted)
                {

                    DateTime currDate = DateTime.Now;
                    TimeTrackingService.SetStartTime(this.patient.id, currDate.ToString("dd.MM.yyyy HH:mm"));
                    Snackbar snackbar_finished = Snackbar.Make(this.FindViewById<LinearLayout>(Resource.Id.dash_root), "Time tracking started. Click 'Done' to end time tracking.", Snackbar.LengthLong);
                    snackbar_finished.Show();
                }
                else
                {
                    Snackbar snackbar_finished = Snackbar.Make(this.FindViewById<LinearLayout>(Resource.Id.dash_root), "Permission to read/write local storage need to save timestamps. :(", Snackbar.LengthLong);
                    snackbar_finished.Show();
                }
            }
            catch (Exception ex)
            {
                Snackbar snackbar_finished = Snackbar.Make(this.FindViewById<LinearLayout>(Resource.Id.dash_root), ex.Message, Snackbar.LengthLong);
                snackbar_finished.Show();
            }
            
        }

        public void FinishTimeTracking()
        {
            TimeTrackingService.DeleteFile();
            Snackbar snackbar_finished = Snackbar.Make(this.FindViewById<LinearLayout>(Resource.Id.dash_root), "Time tracking finished and saved to database.", Snackbar.LengthLong);
            snackbar_finished.Show();
            //TODO: Send data to server via request from the RequestController
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_dash);
            this.btn_TimeTracking = FindViewById<Button>(Resource.Id.btn_TimeTrack);
            this.btn_Camera = FindViewById<ImageView>(Resource.Id.btn_OpenCamera);
            this.btn_Camera.Click += OpenCamera;
            this.textViewFindingsCount = FindViewById<TextView>(Resource.Id.textViewNumberFindings);
            this.timeDisplay = FindViewById<TextView>(Resource.Id.timeDisplay);
            this.findingGallery = FindViewById<LinearLayout>(Resource.Id.findingGallery);
            this.btn_TimeTracking.Click += this.TimeTrackingClicked;
            this.patient = JsonConvert.DeserializeObject<Patient>(Intent.GetStringExtra("Patient"));
            if (this.patient != null)
            {
                FillViewsWithPatientData();
                var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar_patientdetails);
                SetActionBar(toolbar);
                ActionBar.Title = this.patient.firstname+ " " +this.patient.lastname;
                ActionBar.SetHomeButtonEnabled(true);
                ActionBar.SetDisplayHomeAsUpEnabled(true);
            }
            else
            {
                base.OnBackPressed();
            }
            this.InitTimeTracking();
        }

        private void InitTimeTracking()
        {
            this.GetTimeTrackingInfo();
            if (this.isTrackingOtherPatient)
            {
                this.timeDisplay.Text = "Time tracking other patient currently.\n Not allowed to track 2 patients at the same time.";
                this.timeDisplay.Visibility = ViewStates.Visible;
                this.isCurrTrackingTime = false;
                this.btn_TimeTracking.Enabled = false;
            } else
            {
                if (!String.IsNullOrEmpty(this.startedTimestamp))
                {
                    this.btn_TimeTracking.Enabled = true;
                    this.timeDisplay.Text = "Time tracking running since " + this.startedTimestamp;

                    this.btn_TimeTracking.Text = "Done";
                    this.timeDisplay.Visibility = ViewStates.Visible;
                    this.isCurrTrackingTime = true;
                }
                else
                {
                    this.timeDisplay.Visibility = ViewStates.Gone;
                    this.btn_TimeTracking.Text = "Arrived";
                    this.isCurrTrackingTime = false;
                }
            }
        }

        private void OpenCamera(object sender, EventArgs e)
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            try
            {
                Bitmap bitmap = (Bitmap)data.Extras.Get("data");
                AddFindingDialog addDialog = new AddFindingDialog(this, bitmap);
                addDialog.Create();
                addDialog.btn_Cancel.Click += (ene, a) =>
                {
                    addDialog.Dismiss();
                };
                addDialog.btn_Add.Click += (ene, a) =>
                {
                    this.patient.allFindings.Add(new PatientFinding("Finding1", new DateTime(), this.ConvertBitmapIntoByteArray(bitmap)));
                    this.ReloadFindingsGallery();
                    UpdateFindingsTextView();
                    addDialog.Dismiss();
                };
                addDialog.Show();
            }
            catch (Exception ex)
            {
                Logger.ConsLogger.LogExceptionToConsole(this, new Exception("Error getting image from camera!", ex));
            }
        }

        private void UpdateFindingsTextView()
        {
            this.textViewFindingsCount.Text = "Number of Findings: " + this.patient.allFindings.Count;
        }

        private void ShowKeyboard(EditText userInput)
        {
            userInput.RequestFocus();
            InputMethodManager imm = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
            imm.ToggleSoftInput(ShowFlags.Forced, 0);
        }

        private void HideKeyboard(EditText userInput)
        {
            InputMethodManager imm = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
            imm.HideSoftInputFromWindow(userInput.WindowToken, 0);
        }

        private void TimeTrackingClicked(object sender, EventArgs e)
        {
            if (!isCurrTrackingTime)
            {
                this.btn_TimeTracking.Text = "Done";
                this.SetStartTimeTracking();
                isCurrTrackingTime = true;
            }
            else
            {
                this.btn_TimeTracking.Text = "Arrived";
                this.FinishTimeTracking();
                isCurrTrackingTime = false;
            }
            this.InitTimeTracking();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menuitems_dash, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    return true;
                default:
                    UpdatePatientInformationToDatabase();
                    return base.OnOptionsItemSelected(item);
            }
        }

        private void UpdatePatientInformationToDatabase()
        {
            Snackbar snackbar_updating = Snackbar.Make(this.FindViewById<LinearLayout>(Resource.Id.dash_root), "Saving Patient data to database...", Snackbar.LengthLong);
            snackbar_updating.Show();
            this.patient.notes = patientDesc.Text;        
            PatienRequestController.UpdatePatient(this.patient);
            Snackbar snackbar_finished = Snackbar.Make(this.FindViewById<LinearLayout>(Resource.Id.dash_root), "Saving Patient data finished!", Snackbar.LengthLong);
            snackbar_finished.Show();
        }

        private void FillViewsWithPatientData()
        {
            this.txt_patientName = this.FindViewById<TextView>(Resource.Id.currPatientName);
            this.txt_patientAddress = this.FindViewById<TextView>(Resource.Id.currPatientAdress);
            this.img_patient = this.FindViewById<ImageView>(Resource.Id.currPatientImage);
            this.patientDesc = this.FindViewById<EditText>(Resource.Id.currPatientDesc);
            this.txt_patientName.Text = this.patient.FullName();
            this.txt_patientAddress.Text = this.patient.place.ToDisplayString();
            this.patientDesc.Text = this.patient.notes;
            this.img_patient.SetImageBitmap(patient.GetImageAsAndroidBitmap());
            ReloadFindingsGallery();
            UpdateFindingsTextView();
        }

        private void ReloadFindingsGallery()
        {
            if (this.findingGallery.ChildCount > 0)
            {
                this.findingGallery.RemoveAllViews();
            }
            foreach (PatientFinding pf in this.patient.allFindings)
            {
                ImageView imageView = new ImageView(this);
                imageView.SetImageBitmap(pf.GetImageAsAndroidBitmap());
                imageView.Click += (e, a) =>
                {
                    Intent intent = new Intent(this, typeof(FindingActivity));
                    intent.PutExtra("image", pf.GetImageAsAndroidBitmap());
                    this.StartActivity(intent);
                };
                this.findingGallery.AddView(imageView, LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
            }
        }

        private byte[] ConvertBitmapIntoByteArray(Bitmap bitmap)
        {
            byte[] bitmapData;
            using (var stream = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
                bitmapData = stream.ToArray();
            }
            return bitmapData;
        }
    }
}