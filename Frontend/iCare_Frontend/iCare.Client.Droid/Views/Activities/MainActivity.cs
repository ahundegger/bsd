﻿using System;
using Android;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using iCare.Client.Droid.Adapter;
using iCare.Client.Droid.Views.Activities;
using iCare.CoreAccess.Requests;
using iCare.Commons.Entities;
using System.Collections.Generic;
using Android.Content;
using Newtonsoft.Json;
using System.Threading.Tasks;
using iCare.Client.Droid.Cache;

namespace iCare.Client.Droid
{
    [Activity(Label = "Today's Patients", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener
    {
        private RecyclerView recycler;
        private RecyclerView.LayoutManager recyclerLayoutManager;
        private NurseryListAdapter nurseryListAdapter;
        private SwipeRefreshLayout swipeRefreshLayout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            this.swipeRefreshLayout = FindViewById<SwipeRefreshLayout>(Resource.Id.swipeRefreshLayout);
            this.swipeRefreshLayout.Refresh += delegate {
                this.RefreshNurseryList();
            };

            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);

            this.recycler = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            this.recyclerLayoutManager = new LinearLayoutManager(this);
            this.recycler.SetLayoutManager(recyclerLayoutManager);
            //this.nurseryListAdapter = new NurseryListAdapter(NurseRequestController.GetDummyPatients());
            this.nurseryListAdapter = new NurseryListAdapter(CareListRequestController.GetCareListByNurseId(Configuration.CredentialManager.GetNurseId()));
            this.nurseryListAdapter.ItemClick += this.PatientItemClicked;
            this.recycler.SetAdapter(nurseryListAdapter);
        }

        protected override void OnRestart()
        {
            base.OnRestart();
            this.RefreshNurseryList();
        }

        public void RefreshNurseryList()
        {
            this.recyclerLayoutManager = new LinearLayoutManager(this);
            this.recycler.SetLayoutManager(recyclerLayoutManager);
            //this.nurseryListAdapter = new NurseryListAdapter(NurseRequestController.GetDummyPatients());
            this.nurseryListAdapter = new NurseryListAdapter(CareListRequestController.GetCareListByNurseId(Configuration.CredentialManager.GetNurseId()));
            this.nurseryListAdapter.ItemClick += this.PatientItemClicked;
            this.recycler.SetAdapter(nurseryListAdapter);
            this.swipeRefreshLayout.Refreshing = false;
        }

        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            if (drawer.IsDrawerOpen(GravityCompat.Start))
            {
                drawer.CloseDrawer(GravityCompat.Start);
            }
            else
            {
                Android.Support.V7.App.AlertDialog.Builder alert = new Android.Support.V7.App.AlertDialog.Builder(this);
                alert.SetTitle("Logout?");
                alert.SetIcon(Resource.Drawable.icare_logo192);
                alert.SetMessage("Do you really want to logout?");
                alert.SetPositiveButton("YES", (senderAlert, args) =>
                {
                    base.OnBackPressed();
                });
                alert.SetNegativeButton("NO", (senderAlert, args) =>
                {
                });
                Dialog dialog = alert.Create();
                dialog.Show();
            }
        }

        void PatientItemClicked(object sender, int position)
        {
            Intent intent = new Intent(this, typeof(PatientDashboardActivity));
            intent.PutExtra("Patient", JsonConvert.SerializeObject(nurseryListAdapter.GetPatient(position)));
            StartActivity(intent);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {

            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case 1:
                    //TODO: Call the method to get Patients from database again
                    Snackbar snackbar_refresh = Snackbar.Make(this.FindViewById<LinearLayout>(Resource.Layout.activity_main), "Retrieving data from server...", Snackbar.LengthLong);
                    snackbar_refresh.Show();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            int id = item.ItemId;

            if (id == Resource.Id.nav_manage)
            {
                //open the settings activity
                this.StartActivity(typeof(OptionsActivity));
            }
            else if (id == Resource.Id.nav_main)
            {

            }

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);
            return true;
        }
    }
}

