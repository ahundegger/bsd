﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using iCare.Client.Droid.Adapter;
using iCare.Client.Droid.Cache;

namespace iCare.Client.Droid.Views.ViewHolders
{
    public class NurseryListViewHolder : RecyclerView.ViewHolder
    {
        public TextView Name { get; private set; }
        public TextView Address { get; private set; }
        public TextView EstimatedTime { get; private set; }
        public ImageView Image { get; private set; }

        public NurseryListViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            // Locate and cache view references:
            Name = itemView.FindViewById<TextView>(Resource.Id.patientName);
            Address = itemView.FindViewById<TextView>(Resource.Id.patientAddress);
            EstimatedTime = itemView.FindViewById<TextView>(Resource.Id.estimatedTime);
            Image = itemView.FindViewById<ImageView>(Resource.Id.patientImage);

            itemView.Click += (sender, e) => listener(base.LayoutPosition);
        }
    }
}