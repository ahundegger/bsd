﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace iCare.Client.Droid.Configuration
{
    public static class CredentialManager
    {
        private static string Username { get; set; }
        private static string Password { get; set; }
        private static int NurseId { get; set; }

        public static void SetCredentials(string username, string password, int nurseId)
        {
            Username = username;
            Password = password;
            NurseId = nurseId;
        }

        public static int GetNurseId()
        {
            return NurseId;
        }

        public static string GetUsername()
        {
            return Username;
        }

        public static string GetPassword()
        {
            return Password;
        }
    }
}