-- Generiert von Oracle SQL Developer Data Modeler 4.0.3.853
--   am/um:        2018-11-07 18:44:57 MEZ
--   Site:      Oracle Database 11g
--   Typ:      Oracle Database 11g




CREATE TABLE Care_List
  (
    CareList_Id    INTEGER NOT NULL ,
    CareList_Name  VARCHAR2 (30) ,
    CareList_State INTEGER ,
    Nurse_Id       INTEGER NOT NULL
  ) ;
ALTER TABLE Care_List ADD CONSTRAINT Care_List_PK PRIMARY KEY ( CareList_Id ) ;

CREATE TABLE Care_List_Entry
  (
    CareListEntry_Id              INTEGER NOT NULL ,
    CareListEntry_ExpectedArrival DATE ,
    CareList_Id                   INTEGER NOT NULL ,
    Patient_Id                    INTEGER NOT NULL
  ) ;
ALTER TABLE Care_List_Entry ADD CONSTRAINT Care_List_Entry_PK PRIMARY KEY ( CareListEntry_Id ) ;

CREATE TABLE Finding
  (
    Finding_Id   INTEGER NOT NULL ,
    Finding_Name VARCHAR2 (40) ,
    Finding_Date DATE ,
    Patient_Id   INTEGER NOT NULL ,
    Finding_BLOB BLOB
  ) ;
ALTER TABLE Finding ADD CONSTRAINT Finding_PK PRIMARY KEY ( Finding_Id ) ;

CREATE TABLE ManagedCareLists
  (
    CareList_Id INTEGER NOT NULL ,
    Manager_Id  INTEGER NOT NULL
  ) ;
ALTER TABLE ManagedCareLists ADD CONSTRAINT ManagedCareLists_PK PRIMARY KEY ( CareList_Id, Manager_Id ) ;

CREATE TABLE Manager
  (
    Manager_Id   INTEGER NOT NULL ,
    Manager_Name VARCHAR2 (30)
  ) ;
ALTER TABLE Manager ADD CONSTRAINT Manager_PK PRIMARY KEY ( Manager_Id ) ;

CREATE TABLE Nurse
  (
    Nurse_Id    INTEGER NOT NULL ,
    Nurse_Name  VARCHAR2 (40) ,
    Nurse_SSNR  VARCHAR2 (20) ,
    Nurse_State INTEGER
  ) ;
ALTER TABLE Nurse ADD CONSTRAINT Nurse_PK PRIMARY KEY ( Nurse_Id ) ;

CREATE TABLE Patient
  (
    Patient_Nr    INTEGER ,
    Patient_Name  VARCHAR2 (30) ,
    Patient_Notes VARCHAR2 (100) ,
    Patient_State INTEGER ,
    Place_Id      INTEGER NOT NULL ,
    Patient_Id    INTEGER NOT NULL
  ) ;
ALTER TABLE Patient ADD CONSTRAINT Patient_PK PRIMARY KEY ( Patient_Id ) ;

CREATE TABLE Place
  (
    Place_Id   INTEGER NOT NULL ,
    Place_Desc VARCHAR2 (50) ,
    Place_Coordinates MDSYS.SDO_GEOMETRY
  ) ;
ALTER TABLE Place ADD CONSTRAINT Place_PK PRIMARY KEY ( Place_Id ) ;

CREATE TABLE TimeTracking
  (
    TimeTraking_Id        INTEGER NOT NULL ,
    Nurse_Id              INTEGER NOT NULL ,
    Patient_Id            INTEGER NOT NULL ,
    TimeTracking_TimeFrom DATE ,
    TimeTracking_TimeTo   DATE
  ) ;
ALTER TABLE TimeTracking ADD CONSTRAINT TimeTracking_PK PRIMARY KEY ( TimeTraking_Id ) ;

ALTER TABLE Care_List_Entry ADD CONSTRAINT Care_List_Entry_Care_List_FK FOREIGN KEY ( CareList_Id ) REFERENCES Care_List ( CareList_Id ) ;

ALTER TABLE Care_List_Entry ADD CONSTRAINT Care_List_Entry_Patient_FK FOREIGN KEY ( Patient_Id ) REFERENCES Patient ( Patient_Id ) ;

ALTER TABLE Care_List ADD CONSTRAINT Care_List_Nurse_FK FOREIGN KEY ( Nurse_Id ) REFERENCES Nurse ( Nurse_Id ) ;

ALTER TABLE Finding ADD CONSTRAINT Finding_Patient_FK FOREIGN KEY ( Patient_Id ) REFERENCES Patient ( Patient_Id ) ;

ALTER TABLE ManagedCareLists ADD CONSTRAINT ManagedCareLists_Care_List_FK FOREIGN KEY ( CareList_Id ) REFERENCES Care_List ( CareList_Id ) ;

ALTER TABLE ManagedCareLists ADD CONSTRAINT ManagedCareLists_Manager_FK FOREIGN KEY ( Manager_Id ) REFERENCES Manager ( Manager_Id ) ;

ALTER TABLE Patient ADD CONSTRAINT Patient_Place_FK FOREIGN KEY ( Place_Id ) REFERENCES Place ( Place_Id ) ;

ALTER TABLE TimeTracking ADD CONSTRAINT TimeTracking_Nurse_FK FOREIGN KEY ( Nurse_Id ) REFERENCES Nurse ( Nurse_Id ) ;

ALTER TABLE TimeTracking ADD CONSTRAINT TimeTracking_Patient_FK FOREIGN KEY ( Patient_Id ) REFERENCES Patient ( Patient_Id ) ;

CREATE SEQUENCE Care_List_CareList_Id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER Care_List_CareList_Id_TRG BEFORE
  INSERT ON Care_List FOR EACH ROW WHEN (NEW.CareList_Id IS NULL) BEGIN :NEW.CareList_Id := Care_List_CareList_Id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE Care_List_Entry_CareListEntry_ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER Care_List_Entry_CareListEntry_ BEFORE
  INSERT ON Care_List_Entry FOR EACH ROW WHEN (NEW.CareListEntry_Id IS NULL) BEGIN :NEW.CareListEntry_Id := Care_List_Entry_CareListEntry_.NEXTVAL;
END;
/

CREATE SEQUENCE Finding_Finding_Id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER Finding_Finding_Id_TRG BEFORE
  INSERT ON Finding FOR EACH ROW WHEN (NEW.Finding_Id IS NULL) BEGIN :NEW.Finding_Id := Finding_Finding_Id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE Manager_Manager_Id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER Manager_Manager_Id_TRG BEFORE
  INSERT ON Manager FOR EACH ROW WHEN (NEW.Manager_Id IS NULL) BEGIN :NEW.Manager_Id := Manager_Manager_Id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE Nurse_Nurse_Id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER Nurse_Nurse_Id_TRG BEFORE
  INSERT ON Nurse FOR EACH ROW WHEN (NEW.Nurse_Id IS NULL) BEGIN :NEW.Nurse_Id := Nurse_Nurse_Id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE Patient_Patient_Id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER Patient_Patient_Id_TRG BEFORE
  INSERT ON Patient FOR EACH ROW WHEN (NEW.Patient_Id IS NULL) BEGIN :NEW.Patient_Id := Patient_Patient_Id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE TimeTracking_TimeTraking_Id START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER TimeTracking_TimeTraking_Id BEFORE
  INSERT ON TimeTracking FOR EACH ROW WHEN (NEW.TimeTraking_Id IS NULL) BEGIN :NEW.TimeTraking_Id := TimeTracking_TimeTraking_Id.NEXTVAL;
END;
/


-- Zusammenfassungsbericht f�r Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             9
-- CREATE INDEX                             0
-- ALTER TABLE                             18
-- CREATE VIEW                              0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           7
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          7
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
