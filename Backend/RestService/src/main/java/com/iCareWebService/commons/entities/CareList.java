package com.iCareWebService.commons.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.iCareWebService.commons.interfaces.IEntity;

public class CareList implements IEntity {


	private int id = -1;
	private String name = "";	
	private boolean state = false;
	private Nurse assigned = null;
	private List<CareListEntry> careListEntries = null;
	
	public CareList() {
	
	}
		
	public CareList(int id, String name, boolean state, Nurse assigned, List<CareListEntry> careList) {
		super();
		this.id = id;
		this.name = name;
		this.state = state;
		this.assigned = assigned;
		this.careListEntries = careList;
	}
     
	public CareList(CareList copy) {
		super();
		this.id = copy.getId();
		this.name = copy.getName();
		this.state = copy.isState();
		this.assigned = copy.getAssigned();
		this.careListEntries = copy.getCareListEntries();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public Nurse getAssigned() {
		return assigned;
	}

	public void setAssigned(Nurse assigned) {
		this.assigned = assigned;
	}

	public List<CareListEntry> getCareListEntries() {
		return careListEntries;
	}

	public void setCareListEntries(List<CareListEntry> careList) {
		this.careListEntries = careList;
	}
	
}
