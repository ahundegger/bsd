package com.iCareWebService.commons.entities;

import com.iCareWebService.commons.interfaces.IEntity;

public class User implements IEntity{

	private String username = "";
	private String password = "";
	private int id;

	
	public User() {
		
	}
	public User(User copy) {
		this.password = copy.getPassword();
		this.username = copy.getUsername();
		this.id = copy.getId();
	}
	
	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;		
	}
	public User(String username, String password, int userid) {
		super();
		this.username = username;
		this.password = password;
		this.id = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getId() {
		return id;
	}
	public void setId(int userid) {
		this.id = userid;
	}
		
}
