package com.iCareWebService.commons.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.iCareWebService.commons.interfaces.IEntity;


@Entity
public class Place implements IEntity,java.io.Serializable{

	private int id;
	private String description; 	
	private String streetName;
	private int postalCode;		    
	private int houseNr;   
	private String countryName;
    private String cityName;
    private double coordX;
    private double coordY;       
	
	public Place() {
		
	}
	
	
	
	public Place(int id, String description, int coordinates, String streetName, int postalCode, int houseNr,
			String countryName, String cityName, double coordX, double coordY) {
		super();
		this.id = id;
		this.description = description;		
		this.streetName = streetName;
		this.postalCode = postalCode;
		this.houseNr = houseNr;
		this.countryName = countryName;
		this.cityName = cityName;
		this.coordX = coordX;
		this.coordY = coordY;
	}



	public Place(int id, String description, int coordinates) {
		super();
		this.id = id;
		this.description = description;		
	}
	
	public Place(String description, int coordinates) {
		super();
		this.id = -1;
		this.description = description;		
	}
	
	public Place(Place copy) {
		super();
		this.id = copy.getId();
		this.description = copy.getDescription();		
		this.cityName = copy.getCityName();
		this.coordX = copy.getCoordX();
		this.coordY = copy.getCoordY();
		this.houseNr = copy.getHouseNr();
		this.countryName = copy.getCountryName();
		this.postalCode = copy.getPostalCode();
		this.streetName = copy.getStreetName();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}



	public String getStreetName() {
		return streetName;
	}



	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}



	public int getPostalCode() {
		return postalCode;
	}



	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}



	public int getHouseNr() {
		return houseNr;
	}



	public void setHouseNr(int houseNr) {
		this.houseNr = houseNr;
	}



	public String getCountryName() {
		return countryName;
	}



	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}



	public String getCityName() {
		return cityName;
	}



	public void setCityName(String cityName) {
		this.cityName = cityName;
	}



	public double getCoordX() {
		return coordX;
	}



	public void setCoordX(double coordX) {
		this.coordX = coordX;
	}



	public double getCoordY() {
		return coordY;
	}


	public void setCoordY(double coordY) {
		this.coordY = coordY;
	}
	
	
	
}
