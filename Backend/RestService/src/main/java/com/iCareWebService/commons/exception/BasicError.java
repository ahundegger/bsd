package com.iCareWebService.commons.exception;

public class BasicError extends Exception {

	private static final long serialVersionUID = -271582074543512905L;

	private final int businessCode;

	public BasicError(int businessCode, String message) {
		super(message);
		this.businessCode = businessCode;
	}

	public int getBusinessCode() {
		return businessCode;
	}

}
