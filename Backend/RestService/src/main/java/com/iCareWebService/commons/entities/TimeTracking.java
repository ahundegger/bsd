package com.iCareWebService.commons.entities;

import java.util.Date;

public class TimeTracking {

	private int id;
	
	private Patient patient;
	
	private Nurse nurse;
	
	private Date startFrom;
	
	private Date finishedAt;
	

	public TimeTracking(int id, Patient patient, Nurse nurse, Date startFrom, Date endFrom) {
		super();
		this.id = id;
		this.patient = patient;
		this.nurse = nurse;
		this.startFrom = startFrom;
		this.finishedAt = endFrom;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Nurse getNurse() {
		return nurse;
	}

	public void setNurse(Nurse nurse) {
		this.nurse = nurse;
	}

	public Date getStartFrom() {
		return startFrom;
	}

	public void setStartFrom(Date startFrom) {
		this.startFrom = startFrom;
	}

	public Date getEndFrom() {
		return finishedAt;
	}

	public void setEndFrom(Date endFrom) {
		this.finishedAt = endFrom;
	}	
	
}
