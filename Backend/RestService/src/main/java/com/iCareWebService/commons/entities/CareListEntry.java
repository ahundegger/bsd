package com.iCareWebService.commons.entities;

import com.iCareWebService.commons.interfaces.IEntity;

public class CareListEntry implements IEntity {

	private int id;
	private boolean state;
	private CareList carelist = null;
	private Patient patient = null;
	
	public CareListEntry() {
		
	}		

	public CareListEntry(int id, boolean state, CareList carelist, Patient patient) {
		super();
		this.id = id;
		this.state = state;
		this.carelist = carelist;
		this.patient = patient;
	}
	public CareListEntry(CareListEntry copy) {
		super();
		this.id = copy.getId();
		this.state = copy.isState();
		this.carelist = copy.getCarelist();
		this.patient = copy.getPatient();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public CareList getCarelist() {
		return carelist;
	}

	public void setCarelist(CareList carelist) {
		this.carelist = carelist;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	
			
}
