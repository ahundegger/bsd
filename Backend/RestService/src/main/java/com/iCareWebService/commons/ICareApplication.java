package com.iCareWebService.commons;

import org.glassfish.jersey.server.ResourceConfig;

import com.iCareWebService.service.filter.AuthFilter;

public class ICareApplication extends ResourceConfig  {

	public ICareApplication() {		
		// TODO Auto-generated constructor stub		
		packages( "com.iCareWebService.service.filter" );
		register( AuthFilter.class );
	}
	
}
