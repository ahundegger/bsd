package com.iCareWebService.commons.interfaces;

@SuppressWarnings("hiding")
public interface IDal<IEntity> {

	public IEntity create(IEntity newObject) throws Exception;
	
	public IEntity update(IEntity updatedObject) throws Exception;
	
	public Iterable<IEntity> get() throws Exception;
	
	public IEntity get(int id) throws Exception;
	
	public boolean delete(int id) throws Exception;
}
