package com.iCareWebService.commons.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class BasicErrorMapper implements ExceptionMapper<BasicError> {

	public Response toResponse(BasicError bex) {
		return Response.status(bex.getBusinessCode()).entity(bex).build();
	}
}
