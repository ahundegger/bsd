package com.iCareWebService.commons.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.iCareWebService.commons.interfaces.IEntity;


public class Patient implements IEntity,java.io.Serializable{
	
	private int id;
	private String nr;
	private String firstname;
	private String lastname;
	private String notes;
	private int state;
	private Place place;
	private ArrayList<Finding> allFindings;
	private byte[] profileImage;
	
	public Patient() {
		
	}
	
	public Patient(int id,String nr, String firstname, String lastname, String notes, int state, Place street, ArrayList<Finding> allFindings,byte[] profileImage) {
		super();
		this.id = id;
		this.nr = nr;
		this.firstname = firstname;
		this.lastname = lastname;
		this.notes = notes;
		this.state = state;
		this.place = street;
		this.allFindings = allFindings;
		this.profileImage = profileImage;
	}

	public Patient(String nr, String firstname, String lastname, String notes, int state, Place street, ArrayList<Finding> allFindings,byte[] profileImage) {
		super();
		this.id = -1;
		this.nr = nr;
		this.firstname = firstname;
		this.lastname = lastname;
		this.notes = notes;
		this.state = state;
		this.place = street;
		this.allFindings = allFindings;
		this.profileImage = profileImage;
	}
	
	public Patient(Patient copy) {
		super();
		this.id = copy.getId();
		this.nr = copy.getNr();
		this.firstname = copy.getFirstname();
		this.lastname = copy.getLastname();
		this.notes = copy.getNotes();
		this.state = copy.getState();
		this.place = copy.getPlace();
		this.allFindings = copy.getAllFindings();
		this.profileImage = copy.getProfileImage();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public String getNr() {
		return nr;
	}

	public void setNr(String nr) {
		this.nr = nr;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place address) {
		this.place = address;
	}
	
	public ArrayList<Finding> getAllFindings() {
		return allFindings;
	}

	public void setAllFindings(ArrayList<Finding> allFindings) {
		this.allFindings = allFindings;
	}

	public byte[] getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(byte[] profileImage) {
		this.profileImage = profileImage;
	}	
	
	
}
