package com.iCareWebService.commons.entities;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.iCareWebService.commons.interfaces.IEntity;

@Entity
@Table(name ="Finding")
public class Finding implements IEntity,java.io.Serializable{

	private int id;
	private String title;
	private Date creationDate;
	private byte[] data;
	
	public Finding() {
		
	}		
	
	public Finding(String name, Date createDate, byte[] findingBLOB) {
		super();
		this.id = -1;
		this.title = name;
		this.creationDate = createDate;
		this.data = findingBLOB;
	}

	@Id
	@GeneratedValue
	@Column(name="Finding_id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="Finding_title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name="Finding_cerationDate")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name="Finding_data")
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	
		
}
