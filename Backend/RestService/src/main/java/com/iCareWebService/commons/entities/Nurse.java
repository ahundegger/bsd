package com.iCareWebService.commons.entities;

import javax.xml.bind.annotation.XmlRootElement;

import com.iCareWebService.commons.interfaces.IEntity;


public class Nurse implements IEntity,java.io.Serializable{
	
	private int id = -1;
	private String firstname = "";
	private String lastname = "";
	private boolean state = false;
	private int ssnr;
	private User user;
	
	public Nurse() {
	
	}
	
	public Nurse(String nurse_Firstname, String nurse_Lastname, boolean nurse_State, int nurse_ssnr,User nurse_user) {
		super();	
		this.id = -1;
		this.firstname = nurse_Firstname;
		this.lastname = nurse_Lastname;
		this.state = nurse_State;
		this.ssnr = nurse_ssnr;
		this.user = nurse_user;
	}
	public Nurse(int id,String nurse_Firstname, String nurse_Lastname, boolean nurse_State, int nurse_ssnr,User nurse_user) {
		super();
		this.id = id;
		this.firstname = nurse_Firstname;
		this.lastname = nurse_Lastname;
		this.state = nurse_State;
		this.ssnr = nurse_ssnr;
		this.user = nurse_user;
	}
	public Nurse(Nurse copy){
		super();	
		this.id = copy.getId();
		this.firstname = copy.getFirstname();
		this.lastname = copy.getLastname();
		this.state = copy.isState();
		this.ssnr = copy.getSsnr();
		this.user = copy.getUser();
	}
	
	
	public void setId(int nurse_Id) {
		this.id = nurse_Id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String nurse_Firstname) {
		this.firstname = nurse_Firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String nurse_Lastname) {
		this.lastname = nurse_Lastname;
	}


	public void setState(boolean nurse_State) {
		this.state = nurse_State;
	}

	public int getSsnr() {
		return this.ssnr;
	}

	public void setSsnr(int nurse_ssnr) {
		this.ssnr = nurse_ssnr;
	}
	
	public int getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isState() {
		return state;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Nurse[ "+" nurse_id="+id+","+" nurse_firstname="+firstname+","+" nurse_lastname="+lastname+
				","+" nurse_state="+state+","+" nurse_ssnr="+ssnr+"]";
	}
	
	public String toJSON() {
		// TODO Auto-generated method stub
		return "{"+"\"nurse_id\":"+"\""+id+"\","+"\"nurse_firstname\":"+"\""+firstname+"\","+"\"nurse_lastname\":"+"\""+lastname+"\","
				+"\"nurse_state\":"+"\""+state+"\","+"\"nurse_ssnr\":"+"\""+ssnr+"\""+"}";
	}
}
