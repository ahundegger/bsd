package com.iCareWebService.service;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.iCareWebService.commons.entities.CareListEntry;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.manager.logic.CareListEntryManager;

@Path("/carelistEntry")
public class CareListEntryService {

	CareListEntryManager careListEntryManager = new CareListEntryManager();

	@GET
	@Path("/")
	@RolesAllowed({ "ADMIN" })
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CareListEntry> getCareListEntries() throws BasicError {
		List<CareListEntry> retCareListEntries = null;
		try {
			retCareListEntries = (List<CareListEntry>) careListEntryManager.get();
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(), e.getMessage());
		}
		return retCareListEntries;
	}

	@GET
	@Path("/getById/{id}")
	@RolesAllowed({ "USER", "ADMIN" })
	@Produces(MediaType.APPLICATION_JSON)
	public CareListEntry getById(@PathParam("id") int id) throws BasicError {
		CareListEntry retCareListEntry = null;
		try {
			retCareListEntry = careListEntryManager.get(id);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(), e.getMessage());
		}
		return retCareListEntry;
	}

	@POST
	@Path("/create/")
	@RolesAllowed({ "USER", "ADMIN" })
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CareListEntry create(CareListEntry incomingData) throws BasicError {
		CareListEntry retCareListEntry = null;
		try {
			retCareListEntry = careListEntryManager.create(incomingData);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(), e.getMessage());
		}
		return retCareListEntry;
	}

	@PUT
	@Path("/update/")
	@RolesAllowed({ "USER", "ADMIN" })
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CareListEntry update(CareListEntry incomingData) throws BasicError {
		CareListEntry retCareList = null;
		try {
			retCareList = careListEntryManager.update(incomingData);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(), e.getMessage());
		}
		return retCareList;
	}

	@DELETE
	@Path("/{id}")
	@RolesAllowed({ "USER", "ADMIN" })
	@Produces(MediaType.APPLICATION_JSON)
	public boolean delete(@PathParam("id") int id) throws BasicError {
		boolean successfully = false;
		try {
			successfully = careListEntryManager.delete(id);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(), e.getMessage());
		}
		return successfully;
	}
}
