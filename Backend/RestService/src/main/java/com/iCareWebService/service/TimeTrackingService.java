package com.iCareWebService.service;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.iCareWebService.commons.entities.TimeTracking;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.manager.logic.TimeTrackingManager;

@Path("/timetracking")
public class TimeTrackingService {

	TimeTrackingManager timeTrackerManager = new TimeTrackingManager();
	@GET
    @Path("/")
	@RolesAllowed({"USER","ADMIN"})
	@Produces({ MediaType.APPLICATION_JSON })
    public List<TimeTracking> get()throws BasicError
    {     
		List<TimeTracking> allTimeTrackingEntries = null;
        try {
        	allTimeTrackingEntries = (List<TimeTracking>) timeTrackerManager.get();
        }catch(BasicError be){
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicError(500,e.getMessage());
		}
        return allTimeTrackingEntries;
    }
	
	@GET
    @Path("/getById/{id}")
	@RolesAllowed({"USER","ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
    public TimeTracking get(@PathParam("id")int id) throws BasicError
    {      
		TimeTracking ret = null;    		
    		try {    		
    		ret = timeTrackerManager.get(id);
    		}catch(BasicError be){
    			throw be;
    		} catch (Exception e) {
    			e.printStackTrace();
    			throw new BasicError(500,e.getMessage());
    		}
		return ret; 
    }
	
	@POST
    @Path("/create/")
	@RolesAllowed({"USER","ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public TimeTracking create(TimeTracking incomingData)throws BasicError
    {      	
		TimeTracking timeTrackEntryCreated = null;
		try {
			timeTrackEntryCreated = timeTrackerManager.create(incomingData);
		}catch(BasicError be){
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicError(500,e.getMessage());
		}
		return timeTrackEntryCreated;
    }
	
	@PUT
    @Path("/update/")
	@RolesAllowed({"USER","ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public TimeTracking update(TimeTracking incomingData) throws BasicError
    {      
		TimeTracking timeTrackEntryUpdated = null;
		try {
			timeTrackEntryUpdated = timeTrackerManager.update(incomingData);
		}catch(BasicError be){
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicError(500,e.getMessage());
		}
		return timeTrackEntryUpdated;
    }
	
	@DELETE
    @Path("/{id}")
	@RolesAllowed({"USER","ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
    public boolean delete(@PathParam("id")int id) throws BasicError
    {      
		boolean successfully = false;
        try {
        	successfully = timeTrackerManager.delete(id);
        }catch(BasicError be){
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicError(500,e.getMessage());
		}
        return successfully;
    }
}
