package com.iCareWebService.service;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.iCareWebService.commons.entities.Nurse;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.manager.logic.NurseManager;


@Path("/nurse")
public class NurseService {	
	NurseManager nursemanger = new NurseManager();
	
	@GET
	@RolesAllowed({"USER","ADMIN"})
    @Path("/")
	@Produces({ MediaType.APPLICATION_JSON })
    public List<Nurse> getNurses() throws BasicError   {     		        
        List<Nurse> retNurse = null;
		try {
			retNurse = (List<Nurse>) nursemanger.get();
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return retNurse;
    }
	
	@GET
	@RolesAllowed({"USER","ADMIN"})
    @Path("/getById/{id}")
	@Produces(MediaType.APPLICATION_JSON)
    public Nurse getById(@PathParam("id")int id) throws BasicError    {             
        Nurse retNurse = null;
		try {
			retNurse = nursemanger.get(id);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return retNurse;
    }
	
	@POST
	@RolesAllowed({"USER","ADMIN"})
    @Path("/create/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Nurse create(Nurse incomingData) throws BasicError  {     
		Nurse retNurse = null;
		try {
			retNurse = nursemanger.create(incomingData);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return retNurse;
    }
	
	@PUT
	@RolesAllowed({"USER","ADMIN"})
    @Path("/update/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Nurse update(Nurse incomingData) throws BasicError   {      
		Nurse retNurse = null;
		try {
			retNurse = nursemanger.update(incomingData);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return retNurse;
    }
	
	@DELETE
	@RolesAllowed({"USER","ADMIN"})
    @Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
    public boolean delete(@PathParam("id")int id) throws BasicError
    {    
		boolean successfully = false;
		try {
			successfully = nursemanger.delete(id);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return successfully;        
    }
}
