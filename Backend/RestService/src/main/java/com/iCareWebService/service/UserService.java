package com.iCareWebService.service;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import com.iCareWebService.commons.entities.Nurse;
import com.iCareWebService.commons.entities.User;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.commons.exception.BasicErrorMapper;
import com.iCareWebService.manager.logic.NurseManager;
import com.iCareWebService.manager.logic.UserManager;


@Path("/user")
public class UserService {	
	UserManager usermanger = new UserManager();
	NurseManager nursemanager = new NurseManager();
	
	@GET
	@RolesAllowed("ADMIN")
    @Path("/")
	@Produces({ MediaType.APPLICATION_JSON })
    public List<User> getUrses() throws BasicError
    {     
		List<User> retUsers = null;		
        try {
			retUsers = (List<User>) usermanger.get();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return retUsers;
    }
	
	@GET
	@RolesAllowed("ADMIN")
    @Path("/getById/{id}")
	@Produces(MediaType.APPLICATION_JSON)
    public User getById(@PathParam("id")int id) throws BasicError
    {      
		User retUser = null; 
        try {
			retUser = usermanger.get(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return retUser;
    }
	@GET
	@RolesAllowed("ADMIN")
    @Path("/getByUsername/{username}")
	@Produces(MediaType.APPLICATION_JSON)
    public User getById(@PathParam("username")String username) throws BasicError
    {      		
		User retUser = null; 
        try {
			retUser = usermanger.getByUsername(username);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new BasicError(500,e.getMessage());					
		}
        return retUser;
    }
	
	@POST
	@RolesAllowed("ADMIN")
    @Path("/create/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public User create(User incomingData) throws BasicError
    {      	
		User retUser = null;
		try {
			retUser = usermanger.create(incomingData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retUser;
    }
	
	@PUT
	@RolesAllowed("ADMIN")
    @Path("/update/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public User update(User incomingData) throws BasicError
    {      
		User retUser = null;
		try {
			retUser = usermanger.update(incomingData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retUser;
    }
	
	@DELETE
    @Path("/{id}")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
    public boolean delete(@PathParam("id")int id) throws BasicError
    {     boolean retbool = false; 
        try {
			retbool =  usermanger.delete(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return retbool;
    }
	
	@GET
	@PermitAll
    @Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
    public int logIn(@Context HttpHeaders httpheaders) throws BasicError
    {      		
		User retUser = null; 
		int successfully = -1;
		String header = httpheaders.getHeaderString("Authorization");
		header = header.replaceFirst("Basic ", "");
        try {
			retUser = usermanger.logIn(header);
			if(retUser != null ) {
				successfully = nursemanager.getByUserId(retUser.getId()).getId();				
			}						
		}catch(BasicError er ) {
			throw new BasicError(er.getBusinessCode(),er.getMessage());
		}catch (Exception e) {		
			// TODO Auto-generated catch block
			throw new BasicError(500,e.getMessage()+" "+e.toString()+" "+e.getLocalizedMessage());					
		}
        return successfully;
    }
}
