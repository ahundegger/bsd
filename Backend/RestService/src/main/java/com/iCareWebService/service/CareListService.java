package com.iCareWebService.service;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.iCareWebService.commons.entities.CareList;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.manager.logic.CareListManager;

@Path("/carelist")
public class CareListService {

	CareListManager careListmanger = new CareListManager();
	
	@GET
    @Path("/")
	@RolesAllowed({"USER","ADMIN"})
	@Produces({ MediaType.APPLICATION_JSON })
    public List<CareList> getCareList() throws BasicError   {     		        
        List<CareList> retCareList = null;
		try {
			retCareList = (List<CareList>) careListmanger.get();
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return retCareList;
    }
	
	@GET
    @Path("/getById/{id}")
	@RolesAllowed({"USER","ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
    public CareList getById(@PathParam("id")int id) throws BasicError    {             
		CareList retCareList = null;
		try {
			retCareList = careListmanger.get(id);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return retCareList;
    }
	
	@GET
    @Path("/getByNurseId/{id}")
	@RolesAllowed({"USER","ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
    public CareList getByNurseId(@PathParam("id")int id) throws BasicError    {             
		CareList retCareList = null;
		try {
			retCareList = careListmanger.getByNurseId(id);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return retCareList;
    }
	
	@POST
    @Path("/create/")
	@RolesAllowed({"USER","ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public CareList create(CareList incomingData) throws BasicError  {     
		CareList retCareList = null;
		try {
			retCareList = careListmanger.create(incomingData);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return retCareList;
    }
	
	@PUT
    @Path("/update/")
	@RolesAllowed({"USER","ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public CareList update(CareList incomingData) throws BasicError   {      
		CareList retCareList = null;
		try {
			retCareList = careListmanger.update(incomingData);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return retCareList;
    }
	
	@DELETE
    @Path("/{id}")
	@RolesAllowed({"USER","ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
    public boolean delete(@PathParam("id")int id) throws BasicError
    {    
		boolean successfully = false;
		try {
			successfully = careListmanger.delete(id);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return successfully;        
    }
}
