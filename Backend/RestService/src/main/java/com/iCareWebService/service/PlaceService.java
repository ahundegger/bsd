package com.iCareWebService.service;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.imageio.ImageIO;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.iCareWebService.commons.entities.Finding;
import com.iCareWebService.commons.entities.Patient;
import com.iCareWebService.commons.entities.Place;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.manager.logic.PatientManager;
import com.iCareWebService.manager.logic.PlaceManager;

@Path("/place")
public class PlaceService {

	private static PlaceManager pmanager = new PlaceManager();

	@GET
	@Path("/")
	@RolesAllowed({ "USER", "ADMIN" })
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Place> get() throws BasicError {
		List<Place> allPlaces; 
		try {
			 allPlaces = (List<Place>) pmanager.get();
		} catch (BasicError be) {
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicError(500, e.getMessage());
		}
		return allPlaces;
	}

	@GET
	@Path("/getById/{id}")
	@RolesAllowed({ "USER", "ADMIN" })
	@Produces(MediaType.APPLICATION_JSON)
	public Place get(@PathParam("id") int id) throws BasicError {
		Place ret = null;
		try {
			ret = pmanager.get(id);
		} catch (BasicError be) {
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicError(500, e.getMessage());
		}
		return ret;
	}

	@POST
	@Path("/create/")
	@RolesAllowed({ "USER", "ADMIN" })
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Place create(Place incomingData) throws BasicError {
		Place placeCreated;
		try {
			placeCreated = pmanager.create(incomingData);
		} catch (BasicError be) {
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicError(500, e.getMessage());
		}
		return placeCreated;
	}

	@PUT
	@Path("/update/")
	@RolesAllowed({ "USER", "ADMIN" })
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Place update(Place incomingData) throws BasicError {
		Place placeUpdated;
		try {
			placeUpdated = pmanager.update(incomingData);
		} catch (BasicError be) {
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicError(500, e.getMessage());
		}
		return placeUpdated;
	}

	@DELETE
	@Path("/{id}")
	@RolesAllowed({ "USER", "ADMIN" })
	@Produces(MediaType.APPLICATION_JSON)
	public boolean delete(@PathParam("id") int id) throws BasicError {
		try {
			return pmanager.delete(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
