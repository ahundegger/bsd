package com.iCareWebService.service;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.iCareWebService.commons.entities.Finding;
import com.iCareWebService.commons.entities.Patient;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.manager.logic.PatientManager;

@Path("/patient")
public class PatientService  {

	private static PatientManager pmanager = new PatientManager();	
	
	@GET
    @Path("/")
	@RolesAllowed({"USER","ADMIN"})
	@Produces({ MediaType.APPLICATION_JSON })
    public List<Patient> get() throws BasicError
    {     		
		List<Patient> allPatients = null;
        try {
        	allPatients = (List<Patient>) pmanager.get();
        }catch(BasicError be){
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicError(500,e.getMessage());
		}
        return allPatients;        
    }
	
	@GET
    @Path("/getById/{id}")
	@RolesAllowed({"USER","ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
    public Patient get(@PathParam("id")int id) throws BasicError
    {   
		Patient retPatient = null;
			try {
				retPatient = pmanager.get(id);
			} catch (BasicError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new BasicError(e.getBusinessCode(),e.getMessage());
			}
			return retPatient;    	
    }
	
	@POST
    @Path("/create/")
	@RolesAllowed({"USER","ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Patient create(Patient incomingData) throws BasicError
    {      	
		Patient ret = null;		
    	ArrayList<Finding> allFindings = new ArrayList<Finding>();    	
    	try {		    		
    		ret = pmanager.create(incomingData);
		}catch(BasicError be){
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicError(500,e.getMessage());
		}
		return ret; 
    }
	
	@PUT
    @Path("/update/")
	@RolesAllowed({"USER","ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Patient update(Patient incomingData) throws BasicError
    {      
		Patient retPatient = null;
		try {
			retPatient = pmanager.update(incomingData);
		} catch (BasicError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(e.getBusinessCode(),e.getMessage());
		}
		return retPatient;
    }
	
	@DELETE
    @Path("/{id}")
	@RolesAllowed({"USER","ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
    public boolean delete(@PathParam("id")int id) throws BasicError
    {      
		boolean successfully = false;
        try {
        	successfully = pmanager.delete(id);
        }catch(BasicError be){
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicError(500,e.getMessage());
		}
        return successfully;
    }
}
