package com.iCareWebService.dataAccess.mySql;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.iCareWebService.commons.entities.Patient;
import com.iCareWebService.commons.entities.User;
import com.iCareWebService.commons.interfaces.IDal;
import com.iCareWebService.commons.interfaces.IDalPatient;
import com.iCareWebService.dataAccess.DBAccessModule;



public class PatientDALMySQL implements IDal<Patient>,IDalPatient{
	
	@Override
	public Patient create(Patient newObject) throws Exception {
		Session session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
		Transaction tx = null;		
		Patient patient = null;
		
		try {
			tx = session.beginTransaction();
			patient = new Patient(newObject);
			patient.setId((int) session.save(patient));	
			tx.commit();
		}catch (HibernateException e) {
			// TODO: handle exception			
			if(tx!=null) tx.rollback();
			e.printStackTrace();
			throw new Exception("Error Occured: "+ e.getMessage());
		} finally {
			if (session != null && session.isOpen()) {
	             session.close();
	         }
		}
		
		return patient;
	}

	@Override
	public Patient update(Patient updatedObject) throws Exception {
		// TODO Auto-generated method stub
		Session session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
		Transaction tx = null;		
		Patient retPatient = null;		
		try {
			tx = session.beginTransaction();
			retPatient = (Patient) session.get(Patient.class, updatedObject.getId());
			retPatient.setFirstname(updatedObject.getFirstname());
			retPatient.setLastname(updatedObject.getLastname());
			retPatient.setNotes(updatedObject.getNotes());
			retPatient.setState(updatedObject.getState());	
			retPatient.setNr(updatedObject.getNr());
			session.update(retPatient);			
			tx.commit();
		}catch (HibernateException e) {
			// TODO: handle exception			
			if(tx!=null) tx.rollback();
			e.printStackTrace();
			throw new Exception("Error Occured: "+ e.getMessage());
		} finally {
			if (session != null && session.isOpen()) {
	             session.close();
	         }
		}
		
		return retPatient;
			
	}

	@Override
	public Iterable<Patient> get() {
		 Session session = null;
	     List<Patient> retPatients =null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 retPatients = session.createQuery("from Patient").list();
	     } catch (Exception e) {
	         e.printStackTrace();
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retPatients;
	}
	@Override
	public Patient get(int id) throws Exception {
		// TODO Auto-generated method stub
		 Session session = null;
		 Patient retPatient = null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 retPatient = (Patient) session.get(Patient.class, id);
	     } catch (Exception e) {
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retPatient;
	}

	@Override
	public boolean delete(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = null;
	     boolean successfullyDeleted = false;
	     Transaction tx = null;	
	     try {	    	 
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 tx = session.beginTransaction();
	    	 Patient foundedPatient = (Patient) session.get(Patient.class, id);
	    	 if(foundedPatient == null) {
	    		 throw new Exception("Patient with following id not exists: "+ id );
	    	 }
	    	 System.out.println("DBGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG"+foundedPatient.getLastname());
	    	 session.delete(foundedPatient);
	    	 tx.commit();
	    	 successfullyDeleted = true;
	     } catch (Exception e) {
	    	 System.out.println(e.getMessage());
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return successfullyDeleted;
	}

}
