package com.iCareWebService.dataAccess.mySql;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.iCareWebService.commons.entities.Patient;
import com.iCareWebService.commons.entities.Place;
import com.iCareWebService.commons.entities.User;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.commons.interfaces.IDal;
import com.iCareWebService.dataAccess.DBAccessModule;

public class UserDALMySQL implements IDal<User> {

	@Override
	public User create(User newObject) throws Exception {
		// TODO Auto-generated method stub
		Session session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
		Transaction tx = null;
		Integer userID = null;
		User user = null;
		
		try {
			tx = session.beginTransaction();
			user = new User(newObject);
			user.setId((int) session.save(user));	
			tx.commit();
		}catch (HibernateException e) {
			// TODO: handle exception			
			if(tx!=null) tx.rollback();
			e.printStackTrace();
			throw new Exception("Error Occured: "+ e.getMessage());
		} finally {
			if (session != null && session.isOpen()) {
	             session.close();
	         }
		}
		
		return user;
	}

	@Override
	public User update(User updatedObject) throws Exception {
		// TODO Auto-generated method stub
		 Session session = null;
	     User retUser = null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 retUser = (User) session.get(User.class, updatedObject.getId());
	    	 retUser.setPassword(updatedObject.getPassword());
	    	 retUser.setUsername(updatedObject.getUsername());
	    	 session.update(retUser);
	     } catch (Exception e) {
	    	 System.out.println(e.getMessage());
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retUser;
	}

	@Override
	public Iterable<User> get() throws Exception {
		// TODO Auto-generated method stub
		 Session session = null;
	     List<User> retUsers = null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 retUsers = session.createQuery("from User").list();
	     } catch (Exception e) {
	    	 System.out.println(e.getMessage());
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retUsers;
	}

	@Override
	public User get(int id) throws Exception {
		// TODO Auto-generated method stub
		 Session session = null;
	     User retUser = null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 retUser = (User) session.get(User.class, id);
	     } catch (Exception e) {
	    	 System.out.println(e.getMessage());
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retUser;
	}

	@Override
	public boolean delete(int id) throws Exception {
		// TODO Auto-generated method stub
		 Session session = null;
	     boolean successfullyDeleted = false;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 User foundedUser = (User) session.get(User.class, id);
	    	 if(foundedUser == null) {
	    		 throw new Exception("User with following id not exists: "+ id );
	    	 }
	    	 session.delete(foundedUser);
	    	 successfullyDeleted = true;
	     } catch (Exception e) {
	    	 System.out.println(e.getMessage());
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return successfullyDeleted;
	}

	public User getByUsername(String username) throws Exception {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		 Session session = null;
		 User retUser = null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 Query query = session.createQuery("from User where User_username = :username");
	    	 query.setParameter("username", username);
	    	 List<User> foundedUsers = query.list();
	    	if (foundedUsers.size() != 0) {
	    		 retUser = foundedUsers.get(0);	 
	    	}	    	   	
	     } catch (Exception e) {
	    	 System.out.println(e.getMessage());
	         e.printStackTrace();
	        throw e;
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retUser;
	}

}
