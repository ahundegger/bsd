package com.iCareWebService.dataAccess.mySql;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.iCareWebService.commons.entities.Patient;
import com.iCareWebService.commons.entities.Place;
import com.iCareWebService.commons.interfaces.IDal;
import com.iCareWebService.commons.interfaces.IDalPatient;
import com.iCareWebService.commons.interfaces.IDalPlace;
import com.iCareWebService.dataAccess.DBAccessModule;



public class PlaceDALMySQL implements IDal<Place>,IDalPlace{
	
	@Override
	public Place create(Place newObject) {
		Session session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
		Transaction tx = null;
		Integer patientID = null;
		Place place = null;
		System.out.println("DA: "+newObject.getCountryName());
		try {
			tx = session.beginTransaction();
			place = new Place(newObject);
			place.setId((int) session.save(place));
			tx.commit();
		}catch (HibernateException e) {
			// TODO: handle exception
			if(tx!=null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return place;
	}

	@Override
	public Place update(Place updatedObject) {
		// TODO Auto-generated method stub
		return null;		
	}

	@Override
	public Iterable<Place> get() {
		// TODO Auto-generated method stub
		Session session = null;
	     List<Place> places = null;
	     Transaction tx = null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 tx = session.beginTransaction();
	    	 places = (List<Place>) session.createQuery("from Place").list();
	    	 if(places == null) {
	    		 throw new Exception("Something gone wrong");
	    	 }
	    	 tx.commit();
	     } catch (Exception e) {
	         e.printStackTrace();
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return places;
	}

	@Override
	public Place get(int id) {
		 Session session = null;
	     Place place = null;
	     Transaction tx = null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 tx = session.beginTransaction();
	    	 place = (Place) session.get(Place.class, id);
	    	 tx.commit();
	     } catch (Exception e) {
	         e.printStackTrace();
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return place;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

}
