package com.iCareWebService.dataAccess.oracle;

import java.util.ArrayList;
import java.util.List;

import com.iCareWebService.commons.entities.Nurse;
import com.iCareWebService.commons.entities.User;
import com.iCareWebService.commons.interfaces.IDal;
import com.iCareWebService.commons.interfaces.IDalNurse;

public class NurseDALMySQL implements IDal<Nurse>, IDalNurse{

	private static List<Nurse> allnurse = new ArrayList<Nurse>();
	private static int idx = allnurse.size();
	
	public NurseDALMySQL() {
		Nurse nurse1 = new Nurse("Nurse1 Firstname", "Nurse1 Lastname", false, 4320500, new User("nurse1User","dkfsj",5));
		nurse1.setId(1);
		Nurse nurse2 = new Nurse("Nurse2 Firstname", "Nurse2 Lastname", false, 4766000,new User("nurse2User","dkfsj",4));
		nurse2.setId(2);		
		Nurse nurse3 = new Nurse("Nurse3 Firstname", "Nurse3 Lastname", false, 4650000,new User("nurse3User","dkffdssj",3));
		nurse3.setId(3);		
		Nurse nurse4 = new Nurse("Nurse4 Firstname", "Nurse4 Lastname", false, 4324300,new User("nurse4User","dkfdfdsj",2));
		nurse4.setId(4);		
		Nurse nurse5 = new Nurse("Nurse5 Firstname", "Nurse5 Lastname", false, 4320000,new User("nurse5User","dkfsj",1));
		nurse5.setId(5);		
		Nurse nurse6 = new Nurse("Nurse6 Firstname", "Nurse6 Lastname", false, 4320000,new User("nurse6User","dkfsj",6));
		nurse6.setId(6);
		allnurse.add(nurse1);
		allnurse.add(nurse2);
		allnurse.add(nurse3);
		allnurse.add(nurse4);
		allnurse.add(nurse5);
		allnurse.add(nurse6);	
		idx = allnurse.size();
	}
	
	public Nurse create(Nurse object) {
		// TODO Auto-generated method stub
		idx++;
		object.setId(idx);
		allnurse.add(object);
		return object;
	}
	
	
	public Nurse update(Nurse updatedObject) {
		// TODO Auto-generated method stub
		Nurse nurseToUpdate = get(updatedObject.getId());
		nurseToUpdate.setFirstname(updatedObject.getFirstname());
		nurseToUpdate.setLastname(updatedObject.getLastname());
		nurseToUpdate.setSsnr(updatedObject.getSsnr());
		nurseToUpdate.setState(updatedObject.isState());
		return nurseToUpdate;
	}
	
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		Nurse nursetoDelte = get(id);
		return allnurse.remove(nursetoDelte);
	}

	
	public Nurse get(int id) {
		// TODO Auto-generated method stub
		Nurse retNurse = null;
		boolean found = false;
		int i=0;
		while(!found) {
			retNurse = allnurse.get(i);
			if(retNurse.getId() == id ) {
				found = true;
			}
			i++;
		}
		return retNurse;
	}
	
	public List<Nurse> get() {
		// TODO Auto-generated method stub
		return allnurse;
	}
}
