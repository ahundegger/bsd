package com.iCareWebService.dataAccess;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class DBAccessModule {
	private static SessionFactory sessionFactoryObj;
	private static DBAccessModule instance = null;
	
	private DBAccessModule() {
		buildSessionFactory();
	}
	
	
	private static void buildSessionFactory() {
		try {
			Configuration hibConfig = new Configuration().configure("hibernate.cfg.xml");
			ServiceRegistry serviceRegistryObj = new StandardServiceRegistryBuilder().applySettings(hibConfig.getProperties()).build(); 
			sessionFactoryObj = hibConfig.buildSessionFactory(serviceRegistryObj);
		} catch (Throwable e) {
			System.err.println("Failed to create sessionFactory." + e);
			throw new ExceptionInInitializerError(e);
		}	
	}

	public SessionFactory getSessionFactoryObj() {
		return sessionFactoryObj;
	}


	
	public static synchronized DBAccessModule getInstance() {
		if (instance == null) 
            instance = new DBAccessModule(); 
		return instance;
	}
	
	
	
}
