package com.iCareWebService.dataAccess.mySql;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.iCareWebService.commons.entities.CareList;
import com.iCareWebService.commons.entities.CareListEntry;
import com.iCareWebService.commons.entities.Nurse;
import com.iCareWebService.commons.entities.User;
import com.iCareWebService.commons.interfaces.IDal;
import com.iCareWebService.dataAccess.DBAccessModule;

public class CareListDALMySQL implements IDal<CareList>{
	CareListEntryDALMySQL careListEntryDAL = new CareListEntryDALMySQL();

	@Override
	public CareList create(CareList newObject) throws Exception {
		// TODO Auto-generated method stub
		Session session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
		Transaction tx = null;		
		CareList retCareList = null;		
		try {
			tx = session.beginTransaction();
			retCareList = new CareList(newObject);
			retCareList.setId((int) session.save(retCareList));	
			tx.commit();
		}catch (HibernateException e) {
			// TODO: handle exception			
			if(tx!=null) tx.rollback();
			e.printStackTrace();
			throw new Exception("Error Occured: "+ e.getMessage());
		} finally {
			if (session != null && session.isOpen()) {
	             session.close();
	         }
		}
		
		return retCareList;
	}

	@Override
	public CareList update(CareList updatedObject) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<CareList> get() throws Exception {
		// TODO Auto-generated method stub
		 Session session = null;
	     List<CareList> retCarelists = new ArrayList<CareList>();
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 //Hibernate.initialize(session);	    	
	    	 SQLQuery query = (SQLQuery) session.createSQLQuery("Select CareList_id,CareList_state,Nurse_Id,CareList_name from CARELIST");
	    	 List<Object[]> rows = query.list();
	    	 for(Object[] row : rows){
	    	 	CareList careList = new CareList();
	    	 	careList.setId(Integer.parseInt(row[0].toString()));	    	 	
	    	 	careList.setState(Boolean.parseBoolean(row[1].toString()));	    	 
	    	 	Nurse blankNurse = new Nurse();
	    	 	blankNurse.setId(Integer.parseInt(row[2].toString()));	    	 	
	    	 	careList.setAssigned(blankNurse);
	    	 	careList.setName(row[3].toString());	
	    	 	careList.setCareListEntries((List<CareListEntry>) careListEntryDAL.getWithCurrentSession(session, Integer.parseInt(row[0].toString())));
	    	 	retCarelists.add(careList);
	    	 }
	     } catch (Exception e) {
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retCarelists;
	}

	@Override
	public CareList get(int id) throws Exception {
		// TODO Auto-generated method stub
		 Session session = null;
		 CareList retCareList = null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 retCareList = (CareList) session.get(CareList.class, id);
	     } catch (Exception e) {
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retCareList;
	}

	@Override
	public boolean delete(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = null;
	     boolean successfullyDeleted = false;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 CareList foundedCareList = (CareList) session.get(CareList.class, id);
	    	 if(foundedCareList == null) {
	    		 throw new Exception("User with following id not exists: "+ id );
	    	 }
	    	 session.delete(foundedCareList);
	    	 successfullyDeleted = true;
	     } catch (Exception e) {
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return successfullyDeleted;
	}
	
	public CareList getByNurseId(int nurseId) throws Exception {
		// TODO Auto-generated method stub
		 Session session = null;
	     CareList retCarelist = new CareList();
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 //Hibernate.initialize(session);	    	
	    	 SQLQuery query = (SQLQuery) session.createSQLQuery("Select CareList_id,CareList_state,Nurse_Id,CareList_name from CARELIST where Nurse_Id = ?");
	    	 query.setInteger(0, nurseId);
	    	 List<Object[]> rows = query.list();
	    	 for(Object[] row : rows){	    	 	
	    		retCarelist.setId(Integer.parseInt(row[0].toString()));	    	 	
	    		retCarelist.setState(Boolean.parseBoolean(row[1].toString()));	    	 
	    	 	Nurse blankNurse = new Nurse();
	    	 	blankNurse.setId(Integer.parseInt(row[2].toString()));	    	 	
	    	 	retCarelist.setAssigned(blankNurse);
	    	 	retCarelist.setName(row[3].toString());	
	    	 	retCarelist.setCareListEntries((List<CareListEntry>) careListEntryDAL.getWithCurrentSession(session, Integer.parseInt(row[0].toString())));	    	 	
	    	 }
	     } catch (Exception e) {
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retCarelist;
	}
}
