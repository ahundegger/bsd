package com.iCareWebService.dataAccess.mySql;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.iCareWebService.commons.entities.Nurse;
import com.iCareWebService.commons.entities.User;
import com.iCareWebService.commons.interfaces.IDal;
import com.iCareWebService.commons.interfaces.IDalNurse;
import com.iCareWebService.dataAccess.DBAccessModule;

public class NurseDALMySQL implements IDal<Nurse>, IDalNurse{

	private static List<Nurse> allnurse = new ArrayList<Nurse>();
	private static int idx = allnurse.size();
	
	public NurseDALMySQL() {
	}
	
	public Nurse create(Nurse object) throws Exception {
		// TODO Auto-generated method stub
		Session session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
		Transaction tx = null;
		Integer userID = null;
		Nurse retNurse = null;		
		try {
			tx = session.beginTransaction();
			retNurse = new Nurse(object);
			retNurse.setId((int) session.save(retNurse));	
			tx.commit();
		}catch (HibernateException e) {
			// TODO: handle exception			
			if(tx!=null) tx.rollback();
			e.printStackTrace();
			throw new Exception("Error Occured: "+ e.getMessage());
		} finally {
			if (session != null && session.isOpen()) {
	             session.close();
	         }
		}
		
		return retNurse;
	}
	
	
	public Nurse update(Nurse updatedObject) throws Exception {
		Session session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
		Transaction tx = null;
		Integer userID = null;
		Nurse retNurse = null;		
		try {
			tx = session.beginTransaction();
			retNurse = (Nurse) session.get(Nurse.class, updatedObject.getId());
			retNurse.setFirstname(updatedObject.getFirstname());
			retNurse.setLastname(updatedObject.getLastname());
			retNurse.setSsnr(updatedObject.getSsnr());
			retNurse.setState(updatedObject.isState());
			retNurse.setUser(updatedObject.getUser());
			session.update(retNurse);			
			tx.commit();
		}catch (HibernateException e) {
			// TODO: handle exception			
			if(tx!=null) tx.rollback();
			e.printStackTrace();
			throw new Exception("Error Occured: "+ e.getMessage());
		} finally {
			if (session != null && session.isOpen()) {
	             session.close();
	         }
		}
		
		return retNurse;
	
	}
	
	public boolean delete(int id) throws Exception{
		// TODO Auto-generated method stub
		 Session session = null;
	     boolean successfullyDeleted = false;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 Nurse foundedNurse = (Nurse) session.get(Nurse.class, id);
	    	 if(foundedNurse == null) {
	    		 throw new Exception("User with following id not exists: "+ id );
	    	 }
	    	 session.delete(foundedNurse);
	    	 successfullyDeleted = true;
	     } catch (Exception e) {
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return successfullyDeleted;
	}

	
	public Nurse get(int id) throws Exception {
		// TODO Auto-generated method stub
		 Session session = null;
	     Nurse retNurse = null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 retNurse = (Nurse) session.get(Nurse.class, id);
	     } catch (Exception e) {
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retNurse;
	}
	public Nurse getByUserId(int userid) throws Exception {
		// TODO Auto-generated method stub
		 Session session = null;
	     Nurse retNurse = null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 Query query = session.createQuery("from Nurse where User_Id = ?");
	    	 query.setInteger(0, userid);
	    	 retNurse = (Nurse) query.list().get(0);
	     } catch (Exception e) {
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retNurse;
	}
	
	public Iterable<Nurse> get() throws Exception {
		// TODO Auto-generated method stub
		 Session session = null;
	     List<Nurse> retNurse = null;
	     try {
	    	 session = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 retNurse = session.createQuery("from Nurse").list();
	     } catch (Exception e) {
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retNurse;
	}
}
