package com.iCareWebService.dataAccess.mySql;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.iCareWebService.commons.entities.CareList;
import com.iCareWebService.commons.entities.CareListEntry;
import com.iCareWebService.commons.entities.Nurse;
import com.iCareWebService.commons.entities.Patient;
import com.iCareWebService.commons.interfaces.IDal;
import com.iCareWebService.dataAccess.DBAccessModule;

public class CareListEntryDALMySQL implements IDal<CareListEntry>{

	PatientDALMySQL patientDAL = new PatientDALMySQL();
	@Override
	public CareListEntry create(CareListEntry newObject) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CareListEntry update(CareListEntry updatedObject) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<CareListEntry> get() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CareListEntry get(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(int id) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
	
	public Iterable<CareListEntry> getWithCurrentSession(Session session,int careListId) throws Exception {		
	     List<CareListEntry> retCarelistsEntries = new ArrayList<CareListEntry>();
	     try {	    	
	    	 //Hibernate.initialize(session);	    	
	    	Session session2 = DBAccessModule.getInstance().getSessionFactoryObj().openSession();
	    	 SQLQuery query = (SQLQuery) session2.createSQLQuery("SELECT CareListEntry_id,CareListEntry_state,CareListEntry_Patient FROM CARELISTENTRY where CareList_id = ?");
	    	 query.setInteger(0, careListId);
	    	 List<Object[]> rows = query.list();
	    	 for(Object[] row : rows){
	    	 	CareListEntry careListEntry = new CareListEntry();
	    	 	careListEntry.setId(Integer.parseInt(row[0].toString()));	    	 	
	    	 	careListEntry.setState(Boolean.parseBoolean(row[1].toString()));	    	 	    	 	
	    	 	careListEntry.setPatient(patientDAL.get(Integer.parseInt(row[2].toString())));
	    	 	retCarelistsEntries.add(careListEntry);
	    	 }
	     } catch (Exception e) {
	         e.printStackTrace();
	         throw new Exception("Error Occured: "+ e.getMessage());
	     } finally {
	         if (session != null && session.isOpen()) {
	             session.close();
	         }
	     }
	    return retCarelistsEntries;
	}

}
