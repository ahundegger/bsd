package com.iCareWebService.manager.logic;

import com.iCareWebService.commons.entities.CareList;
import com.iCareWebService.commons.entities.CareListEntry;
import com.iCareWebService.commons.entities.Nurse;
import com.iCareWebService.commons.entities.Patient;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.commons.interfaces.IManager;
import com.iCareWebService.dataAccess.mySql.CareListEntryDALMySQL;
import com.iCareWebService.dataAccess.mySql.PatientDALMySQL;

public class CareListEntryManager implements IManager<CareListEntry> {

	CareListEntryDALMySQL careListDAL = new CareListEntryDALMySQL();
	PatientDALMySQL patientDAL = new PatientDALMySQL();
	@Override
	public CareListEntry create(CareListEntry newObject) throws BasicError {
		// TODO Auto-generated method stub
		CareListEntry retObj = null;
		try {
			Patient dbPatient = patientDAL.get(newObject.getPatient().getId());
			if (dbPatient == null) {
				throw new BasicError(405, "patient doesn't exists");
			} else {
				newObject.setPatient(dbPatient);
				retObj = careListDAL.create(newObject);
			}
		} catch (Exception ex) {
			throw new BasicError(500, ex.getMessage());
		}
		return retObj;
	}

	@Override
	public CareListEntry update(CareListEntry updatedObject) throws BasicError {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<CareListEntry> get() throws BasicError {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CareListEntry get(int id) throws BasicError {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(int id) throws BasicError {
		// TODO Auto-generated method stub
		return false;
	}

}
