package com.iCareWebService.manager.logic;

import com.iCareWebService.commons.entities.Patient;
import com.iCareWebService.commons.entities.Place;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.commons.interfaces.IManager;
import com.iCareWebService.dataAccess.mySql.PatientDALMySQL;

public class PatientManager implements IManager<Patient> {
	private static PatientDALMySQL myDal = new PatientDALMySQL();
	private static PlaceManager placeManager = new PlaceManager();

	@Override
	public Patient create(Patient newObject) throws Exception {
		// Patient is related to Place so we need to call Place first.
		Patient retObj = null;
		try {
			System.out.println("Manager:"+ newObject.getFirstname()+ newObject.getPlace().getCityName()+" ");
			// assume that place already exists in db
			Place dbPlace = placeManager.get(newObject.getPlace().getId());			
			if (dbPlace != null) {
				System.out.println(dbPlace.getCityName());
				newObject.setPlace(dbPlace);
				retObj = myDal.create(newObject);
			} else {
				System.out.println("NULLLLLLLLLL");
				newObject.setPlace(placeManager.create(newObject.getPlace()));
				retObj = myDal.create(newObject);
			}
		} catch (Exception e) {
			throw e;
		}
		return retObj;
	}

	@Override
	public Patient update(Patient updatedObject) throws BasicError {
		// TODO Auto-generated method stub
		Patient retPatient = null;
		try {
			retPatient = myDal.update(updatedObject);
		} catch (Exception e) {
			// TODO: handle exception
			throw new BasicError(500, e.getMessage());
		}
		return retPatient;
	}

	@Override
	public Iterable<Patient> get() throws Exception {
		// TODO Auto-generated method stub
		return myDal.get();
	}

	@Override
	public Patient get(int id) throws BasicError {
		// TODO Auto-generated method stub
		try {
			return myDal.get(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BasicError(500, e.getMessage());
		}
	}

	@Override
	public boolean delete(int id) throws Exception {
		// TODO Auto-generated method stub
		try {
			return myDal.delete(id);
		} catch (Exception ex) {
			throw ex;
		}
	}

}
