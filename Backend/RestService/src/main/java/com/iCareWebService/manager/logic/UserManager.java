package com.iCareWebService.manager.logic;

import org.glassfish.jersey.internal.util.Base64;

import com.iCareWebService.commons.entities.Patient;
import com.iCareWebService.commons.entities.Place;
import com.iCareWebService.commons.entities.User;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.commons.interfaces.IManager;
import com.iCareWebService.dataAccess.mySql.UserDALMySQL;

public class UserManager implements IManager<User>{

	private static UserDALMySQL myDal = new UserDALMySQL();	

	@Override
	public User create(User newObject) throws Exception {		
		User retObj = null;
		try {
			//assume that place already exists in db
			retObj = myDal.create(newObject);
		} catch (Exception e) {
			throw e;
		}
		return retObj;
	}

	@Override
	public User update(User updatedObject) throws Exception {
		// TODO Auto-generated method stub
		return myDal.update(updatedObject);
	}

	@Override
	public Iterable<User> get() throws Exception {
		// TODO Auto-generated method stub
		return myDal.get();
	}

	@Override
	public User get(int id) throws Exception {
		// TODO Auto-generated method stub
		return myDal.get(id);
	}

	@Override
	public boolean delete(int id) throws Exception {
		// TODO Auto-generated method stub
		return myDal.delete(id);
	}
	
	public User getByUsername(String username) throws Exception {
		User retObj = null;
		try {
			//assume that place already exists in db
			retObj = myDal.getByUsername(username);
		} catch (Exception e) {
			throw e;
		}
		return retObj;
	}
	
	public User logIn(String encodedHeader) throws Exception {
		User retObj = null;
		try {
			//assume that place already exists in db
			String usernameAndPassword = new String(Base64.decode(encodedHeader.getBytes()));
			 String[] splittedUsernameAndPassword = usernameAndPassword.split(":");
             String username = splittedUsernameAndPassword[0];
             String password = splittedUsernameAndPassword[1];
			retObj = myDal.getByUsername(username);
			if(retObj == null) {
				throw new BasicError(401, "Username or Password was incorrect");
			}
			if(!retObj.getPassword().equals(password)) {
				throw new BasicError(401, "Username or Password was incorrect");
			}
		} catch (Exception e) {
			throw e;
		}
		return retObj;
	}
}
