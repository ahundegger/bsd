package com.iCareWebService.manager.logic;

import com.iCareWebService.commons.entities.Place;
import com.iCareWebService.commons.interfaces.IManager;
import com.iCareWebService.dataAccess.mySql.PlaceDALMySQL;

public class PlaceManager implements IManager<Place> {

	private static PlaceDALMySQL myDal = new PlaceDALMySQL();

	@Override
	public Place create(Place newObject) throws Exception {
		return myDal.create(newObject);
	}

	@Override
	public Place update(Place updatedObject) throws Exception {
		return myDal.update(updatedObject);
	}

	@Override
	public Iterable<Place> get() throws Exception {
		return myDal.get();
	}

	@Override
	public Place get(int id) throws Exception {
		try {
			return myDal.get(id);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean delete(int id) throws Exception {
		// TODO Auto-generated method stub
		try {
			return myDal.delete(id);
		} catch (Exception e) {
			throw e;
		}
	}

}
