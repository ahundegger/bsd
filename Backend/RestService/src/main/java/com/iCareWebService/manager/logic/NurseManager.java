package com.iCareWebService.manager.logic;

import java.util.List;

import com.iCareWebService.commons.entities.Nurse;
import com.iCareWebService.commons.entities.User;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.commons.interfaces.IManager;
import com.iCareWebService.dataAccess.mySql.NurseDALMySQL;

public class NurseManager implements IManager<Nurse> {

	private static NurseDALMySQL myDal = new NurseDALMySQL();
	private static UserManager usermanager = new UserManager();

	public NurseManager() {
		super();

	}

	@Override
	public Nurse create(Nurse newObject) throws BasicError {
		// TODO Auto-generated method stub
		Nurse retObj = null;
		try {
			User dbUser = usermanager.getByUsername(newObject.getUser().getUsername());
			if (dbUser != null) {
				throw new BasicError(405, "Username exists already");
			} else {
				newObject.setUser(usermanager.create(newObject.getUser()));
				retObj = myDal.create(newObject);
			}
		} catch (Exception ex) {
			throw new BasicError(500, ex.getMessage());
		}
		return retObj;
	}

	@Override
	public Nurse update(Nurse updatedObject) throws BasicError {
		// TODO Auto-generated method stub
		Nurse retNurse = null;
		try {
			User dbUser = usermanager.getByUsername(updatedObject.getUser().getUsername());
			if (dbUser != null) {
				throw new BasicError(405, "Username exists already");
			} else {
			retNurse = myDal.update(updatedObject);
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new BasicError(500, e.getMessage());
		}
		return retNurse;
	}

	@Override
	public boolean delete(int id) throws BasicError {	
		boolean successfully = false;
		try {
			successfully = myDal.delete(id);
		} catch (BasicError bex) {
			throw bex;
		} catch (Exception e) {
			// TODO: handle exception
			throw new BasicError(500, e.getMessage());
		}
		return successfully;
	}

	@Override
	public Nurse get(int id) throws BasicError {
		// TODO Auto-generated method stub
		Nurse retNurse = null;
		try {
			retNurse = myDal.get(id);
			;
		} catch (Exception e) {
			// TODO: handle exception
			throw new BasicError(500, e.getMessage());
		}
		return retNurse;
	}
	
	public Nurse getByUserId(int userid) throws BasicError {
		// TODO Auto-generated method stub
		Nurse retNurse = null;
		try {
			retNurse = myDal.getByUserId(userid);
			;
		} catch (Exception e) {
			// TODO: handle exception
			throw new BasicError(500, e.getMessage());
		}
		return retNurse;
	}
	

	@Override
	public Iterable<Nurse> get() throws BasicError {
		// TODO Auto-generated method stub
		Iterable<Nurse> retNurses = null;
		try {
			retNurses = myDal.get();
		} catch (Exception e) {
			// TODO: handle exception
			throw new BasicError(500, e.getMessage());
		}
		return retNurses;
	}

}
