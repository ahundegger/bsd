package com.iCareWebService.manager.logic;

import com.iCareWebService.commons.entities.CareList;
import com.iCareWebService.commons.entities.Nurse;
import com.iCareWebService.commons.entities.User;
import com.iCareWebService.commons.exception.BasicError;
import com.iCareWebService.commons.interfaces.IManager;
import com.iCareWebService.dataAccess.mySql.CareListDALMySQL;
import com.iCareWebService.dataAccess.mySql.NurseDALMySQL;

public class CareListManager implements IManager<CareList> {
	private static CareListDALMySQL myDal = new CareListDALMySQL();	
	private static NurseManager nursemanger = new NurseManager();
	
	public CareListManager() {
		super();

	}

	@Override
	public CareList create(CareList newObject) throws BasicError {
		// TODO Auto-generated method stub
		CareList retObj = null;
		try {
			Nurse dbNurse = nursemanger.get(newObject.getAssigned().getId());
			if (dbNurse == null) {
				throw new BasicError(405, "Nurse doesn't exists");
			} else {
				newObject.setAssigned(dbNurse);
				retObj = myDal.create(newObject);
			}
		} catch (Exception ex) {
			throw new BasicError(500, ex.getMessage());
		}
		return retObj;
	}

	@Override
	public CareList update(CareList updatedObject) throws BasicError {
		// TODO Auto-generated method stub
		CareList retNurse = null;
		try {
			Nurse dbNurse = nursemanger.get(updatedObject.getAssigned().getId());
			if (dbNurse == null) {
				throw new BasicError(405, "Nurse doesn't exists");
			} else {
			retNurse = myDal.update(updatedObject);
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new BasicError(500, e.getMessage());
		}
		return retNurse;
	}

	@Override
	public boolean delete(int id) throws BasicError {	
		boolean successfully = false;
		try {
			successfully = myDal.delete(id);
		} catch (BasicError bex) {
			throw bex;
		} catch (Exception e) {
			// TODO: handle exception
			throw new BasicError(500, e.getMessage());
		}
		return successfully;
	}

	@Override
	public CareList get(int id) throws BasicError {
		// TODO Auto-generated method stub
		CareList retNurse = null;
		try {
			retNurse = myDal.get(id);
		} catch (Exception e) {
			// TODO: handle exception
			throw new BasicError(500, e.getMessage());
		}
		return retNurse;
	}
	
	public CareList getByNurseId(int id) throws BasicError {
		// TODO Auto-generated method stub
		CareList retNurse = null;
		try {
			retNurse = myDal.getByNurseId(id);
		} catch (Exception e) {
			// TODO: handle exception
			throw new BasicError(500, e.getMessage());
		}
		return retNurse;
	}

	@Override
	public Iterable<CareList> get() throws BasicError {
		// TODO Auto-generated method stub
		Iterable<CareList> retNurses = null;
		try {
			retNurses = myDal.get();
		} catch (Exception e) {
			// TODO: handle exception
			throw new BasicError(500, e.getMessage());
		}
		return retNurses;
	}

}
